# Tests for [Километры добра](http://kilometry.by/) service

The website is under development. This repository contains Acceptance Tests for the website. Please look at 
**acceptance** subproject.

Probably, other tests will be placed here later (for example, tests for integration with on-line payment systems).