Meta:
@logout
@user


Narrative:
As a user
I want to log out from the site
So that I can stop using the service


Scenario: Log out
Given user is logged in
When user clicks on user icon button
And user clicks 'Выйти' button
Then home page is displayed
And a field for entering e-mail is appeared on home page
And a field for entering password is appeared on home page
And 'Войти' button is appeared on home page