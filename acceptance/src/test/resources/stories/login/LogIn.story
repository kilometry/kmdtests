Meta:
@login
@user


Narrative:
As a user
I want to log in on the site
So that I can use the service


Scenario: Show separate login form to user
Given home page is displayed
When user clicks on user icon button
And user clicks 'Вход' button
Then separate login form is displayed
And the input fields on login form are:
|Name|
|Email|
|Пароль|
And the buttons on login form are:
|Name|
|Забыли пароль?|
|Войти|
And the links on login form are:
|Text|
|как благотворитель|
|как семья с особенным ребёнком|


Scenario: Log in via separate login form
Given user is registered on the site
And separate login form is displayed
When user types his e-mail on login form
And user types his password on login form
And user submits login form
Then greeting is updated with user's name
Then user icon button is updated with user's name


Scenario: Log in just via home page
Given user is registered on the site
And home page is displayed
When user types his e-mail on home page
And user types his password on home page
And user clicks 'Войти' button on home page
Then greeting is updated with user's name
Then user icon button is updated with user's name


Scenario: Log in using non-registered email
Given home page is displayed
When user types non-registered e-mail on home page
And user types his password on home page
And user clicks 'Войти' button on home page
Then 'Неверный email или пароль.' alert is appeared


Scenario: Log in using invalid password
Given user is registered on the site
And home page is displayed
When user types his e-mail on home page
And user types invalid password on home page
And user clicks 'Войти' button on home page
Then 'Неверный email или пароль.' alert is appeared


Scenario: Log in using non-activated account
Given user had registered an account but had not activated it
And home page is displayed
When user types his e-mail on home page
And user types his password on home page
And user clicks 'Войти' button on home page
Then 'Ваша учётная запись не была активирована.' alert is appeared
