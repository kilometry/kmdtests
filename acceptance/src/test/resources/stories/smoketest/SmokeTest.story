Meta:
@smoketest


Narrative:
As a tester
I want to perform chains of several scenarios
So that I can test the site as a whole quickly


!-- As long as it's smoke test it breaks BDD conventions. Given-When-Then formula is used here only for running bunches
!-- of steps from other scenarios.


Scenario: Benefator's registration -> activation -> login -> logout
Given smoketest for benefactor - start
When smoketest for benefactor - registration
And smoketest for benefactor - activation
Then smoketest for benefactor - login
And smoketest for benefactor - logout