Meta:
@payment
@benefactor
@bepaid


Narrative:
As a benefactor
I want to use "bePaid" service
So that I can make payments with my credit card


Scenario: Redirect benefactor to bePaid website
Given benefactor is registered on the site
And benefactor is logged in
And payment page is displayed
And 'bePaid' payment method is selected on payment page
When benefactor enters amount of payment on payment page
And benefactor selects 'Я согласен с условиями' checkbox
And benefactor clicks 'Оплатить' button on payment page
Then benefactor is redirected to bePaid website


Scenario: Finish payment successully
Given benefactor is redirected to bePaid website
And benefactor has filled valid credit card details on bePaid website
When benefactor submits payment on bePaid website
Then 'ТРАНЗАКЦИЯ ПРОШЛА УСПЕШНО' message is displayed on bePaid website
And 'Продолжить' button is displayed on bePaid website


Scenario: Proceed to the website after successful payment
Given payment via bePaid website has been finished successfully
When benefactor clicks 'Продолжить' button on bePaid website
Then benefactor is redirected to the website
And payment result page is displayed
And amount of payment is shown on payment result page
And payment method is shown on payment result page
And status of the payment on payment result page is 'Успешно'
And 'Перейти к списку платежей' link is shown on payment result page


Scenario: Finish payment unsuccessully
Given benefactor is redirected to bePaid website
And benefactor has filled invalid credit card details on bePaid website
When benefactor submits payment on bePaid website
Then 'ТРАНЗАКЦИЯ БЫЛА ОТКЛОНЕНА' message is displayed on bePaid website
And 'Продолжить' button is displayed on bePaid website


Scenario: Proceed to the website after unsuccessful payment
Given payment via bePaid website has been finished unsuccessfully
When benefactor clicks 'Продолжить' button on bePaid website
Then benefactor is redirected to the website
And payment result page is displayed
And amount of payment is shown on payment result page
And payment method is shown on payment result page
And status of the payment on payment result page is 'Отказ'
And 'Перейти к списку платежей' link is shown on payment result page