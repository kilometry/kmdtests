Meta:
@payment
@benefactor


Narrative:
As a benefactor
I want to pay money
So that I can buy kilometres


Scenario: Show payment page to benefactor
Given benefactor is registered on the site
And benefactor is logged in
And home page is displayed
When benefactor clicks 'Оплатить поездку' link on home page
Then payment page is displayed
And payment page contains drop-down for payment method
And 'bePaid' payment method is selected by default
And payment page contains input field for payment amount
And payment page contains 'Я согласен с условиями' checkbox
And 'Я согласен с условиями' checkbox is not selected by default
And payment page contains 'Оплатить' button


Scenario: Increase balance of kilometers after successfull payment
Given benefactor is registered on the site
And benefactor is logged in
And profile page for benefactor is displayed
And benefactor sees balance of kilometers in profile
When benefactor makes successful payment
And benefactor opens profile page again
Then balance of kilometers is increased proportionally to amount of the payment


Scenario: Don't change balance of kilometres after failed payment
Given benefactor is registered on the site
And benefactor is logged in
And profile page for benefactor is displayed
And benefactor sees balance of kilometers in profile
When benefactor makes failed payment
And benefactor opens profile page again
Then balance of kilometers is left unchanged in profile
