Meta:
@registration
@family


Narrative:
As a family
I want to register on the site
So I can make the journeys


Scenario: Send e-mail for account activation
Given registration form for family is displayed
When family fills all the mandatory fields on registration form
And family submits registration form
Then 'Регистрация завершена' message is displayed
And e-mail with a link for account activation is sent


Scenario: Activate account
Given family has received a link for account activation
When family follows the link for account activation
Then e-mail confirmed page is displayed
And e-mail confirmed page contains 'Учетная запись активирована' message
