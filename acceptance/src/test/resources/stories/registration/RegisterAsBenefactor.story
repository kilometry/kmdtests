Meta:
@registration
@benefactor


Narrative:
As a benefactor
I want to register on the site
So I can raise the kilometres


Scenario: Send e-mail for account activation
Given registration form for benefactor is displayed
When benefactor fills all the mandatory fields on registration form
And benefactor submits registration form
Then 'Регистрация завершена' message is displayed
And e-mail with a link for account activation is sent


Scenario: Activate account
Given benefactor has received a link for account activation
When benefactor follows the link for account activation
Then e-mail confirmed page is displayed
And e-mail confirmed page contains 'Учетная запись активирована' message
