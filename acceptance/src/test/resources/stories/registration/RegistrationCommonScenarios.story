Meta:
@registration
@user


Narrative:
As a user
I want to register on the site
So that I can use the service


Scenario: Show registration form to user
Given home page is displayed
When user clicks on user icon button
And user clicks 'Регистрация' button
Then registration form is displayed
And the radio buttons on registration form are:
|Name|
|Благотворитель|
|Семья с особенным ребёнком|
And benefactor radio button is selected by default on registration form
And the input fields on registration form are:
|Name|
|Ваша фамилия*:|
|Ваше имя*:|
|Ваше отчество*:|
|Email, по которому с вами можно связаться*:|
|Номер телефона*:|
|Пароль*:|
|Повторите пароль*|
And the buttons on registration form are:
|Name|
|Зарегистрироваться|
|Войти|


Scenario: Show registration form to benefactor directly from home page
Given home page is displayed
When user clicks 'как благотворитель' link on home page
Then registration form is displayed
And benefactor radio button is selected by default on registration form


Scenario: Show registration form to family directly from home page
Given home page is displayed
When user clicks 'как семья с особенным ребенком' link on home page
Then registration form is displayed
And family radio button is selected by default on registration form