package by.kilometry.kmdtests.acceptance.pages;

import by.kilometry.kmdtests.acceptance.pages.parts.LoginForm;
import by.kilometry.kmdtests.acceptance.pages.parts.RegistrationForm;
import by.kilometry.kmdtests.acceptance.pages.parts.SeparateLoginForm;
import by.kilometry.kmdtests.acceptance.pages.parts.UserIconButton;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.slf4j.Logger;

import static by.kilometry.kmdtests.acceptance.framework.util.PropertiesFileLoader.load;
import static java.lang.invoke.MethodHandles.lookup;
import static org.slf4j.LoggerFactory.getLogger;

public class Home extends PageObject {
    private static final Logger LOG = getLogger(lookup().lookupClass());

    @Override
    public void setDefaultBaseUrl(String defaultBaseUrl) {
        String url = load("site.properties").getString("home");
        LOG.debug("URL to home page is [{}].", url);
        super.setDefaultBaseUrl(url);
    }

    public UserIconButton userIconButton() {
        return new UserIconButton(find(By.id("user-icon-button")));
    }

    public LoginForm loginForm() {
        return new LoginForm(find(By.cssSelector("div.login__content-container")));
    }

    public SeparateLoginForm separateLoginForm() {
        return new SeparateLoginForm(divModalContent());
    }

    public RegistrationForm registrationForm() {
        return new RegistrationForm(divModalContent());
    }

    public WebElementFacade message() {
        return find(By.cssSelector("div.modal-wrap-title"));
    }

    public WebElementFacade payForJourneyLink() {
        return find(By.cssSelector("a[href='#/ride-payment']"));
    }

    private WebElementFacade divModalContent() {
        return find(By.cssSelector("div.modal-content"));
    }
}
