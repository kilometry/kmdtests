package by.kilometry.kmdtests.acceptance.pages.payments;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class Payment extends PageObject {
    public WebElementFacade methodDropdown() {
        return find(By.id("method"));
    }

    public WebElementFacade defaultMethod() {
        return methodDropdown().find(By.xpath("..//button"));
    }

    public WebElementFacade amount() {
        return find(By.id("amount"));
    }

    public WebElementFacade agreedCheckbox() {
        return find(By.id("agreedToConditions"));
    }

    public WebElementFacade payForButton() {
        return find(By.xpath(".//button[contains(text(),'Оплатить')]"));
    }
}
