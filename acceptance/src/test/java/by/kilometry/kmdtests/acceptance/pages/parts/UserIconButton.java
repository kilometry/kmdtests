package by.kilometry.kmdtests.acceptance.pages.parts;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

import static java.lang.String.format;

public class UserIconButton extends Part {
    public UserIconButton(WebElementFacade root) {
        super(root);
    }

    public WebElementFacade login() {
        return buttonWithText("Вход");
    }

    public WebElementFacade logout() {
        return buttonWithText("Выйти");
    }

    public WebElementFacade registration() {
        return buttonWithText("Регистрация");
    }

    public WebElementFacade profile() {
        return root.find(By.xpath("//a[contains(text(),'Переводы')]"));
    }

    public WebElementFacade userName() {
        return root.find(By.cssSelector("div.user-name"));
    }

    private WebElementFacade buttonWithText(String text) {
        // Unexpectedly, some buttons (that are visually located under user icon)
        // are not children in DOM. This is why we start searching for element
        // from the root.
        String expression = format("//button[contains(text(),'%s')]", text);
        return root.find(By.xpath(expression));
    }
}