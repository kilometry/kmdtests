package by.kilometry.kmdtests.acceptance.stories.smoketest;

import by.kilometry.kmdtests.acceptance.framework.data.User;
import by.kilometry.kmdtests.acceptance.framework.session.ScenarioSession;
import net.serenitybdd.jbehave.SerenityStory;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.USER;

public class SmokeTest extends SerenityStory {
    @Given("smoketest for benefactor - start")
    public void givenSmoketestForBenefactorStart() {
        User benefactor = User.fromPropertiesFile("users/non-registered/benefactor.properties");
        ScenarioSession.set(USER, benefactor);
    }

    @When("smoketest for benefactor - registration")
    @Composite(steps = {
            "Given home page is displayed",
            "When user clicks 'как благотворитель' link on home page",
            "Then registration form is displayed",
            "Then benefactor radio button is selected by default on registration form",
            "When benefactor fills all the mandatory fields on registration form",
            "When benefactor submits registration form",
            "Then 'Регистрация завершена' message is displayed",
            "Then e-mail with a link for account activation is sent"
    })
    public void whenSmoketestForBenefactorRegistration() {
    }


    @When("smoketest for benefactor - activation")
    @Composite(steps = {
            "When benefactor follows the link for account activation",
            "Then e-mail confirmed page is displayed",
            "Then e-mail confirmed page contains 'Учетная запись активирована' message"
    })
    public void whenSmoketestForBenefactorActivation() {
    }

    @Then("smoketest for benefactor - login")
    @Composite(steps = {
            "Given home page is displayed",
            "When user types his e-mail on home page",
            "When user types his password on home page",
            "When user clicks 'Войти' button on home page",
            "Then greeting is updated with user's name",
            "Then user icon button is updated with user's name"
    })
    public void thenSmoketestForBenefactorLogin() {
    }

    @Then("smoketest for benefactor - logout")
    @Composite(steps = {
            "When user clicks on user icon button",
            "When user clicks 'Выйти' button",
            "Then home page is displayed",
            "Then a field for entering e-mail is appeared on home page",
            "Then a field for entering password is appeared on home page",
            "Then 'Войти' button is appeared on home page"
    })
    public void thenSmoketestForBenefactorLogout() {
    }
}
