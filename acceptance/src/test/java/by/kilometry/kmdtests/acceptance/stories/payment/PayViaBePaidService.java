package by.kilometry.kmdtests.acceptance.stories.payment;

import by.kilometry.kmdtests.acceptance.framework.data.CreditCard;
import by.kilometry.kmdtests.acceptance.framework.data.Payment;
import by.kilometry.kmdtests.acceptance.framework.session.ScenarioSession;
import by.kilometry.kmdtests.acceptance.pages.Pages;
import by.kilometry.kmdtests.acceptance.pages.payments.BePaid;
import by.kilometry.kmdtests.acceptance.pages.payments.PaymentResult;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static by.kilometry.kmdtests.acceptance.framework.data.CreditCard.fromPropertiesFile;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.Byn.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text.Text.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.matcher.Url.hostIs;
import static by.kilometry.kmdtests.acceptance.framework.matcher.Url.urlContains;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.PAYMENT_AMOUNT;
import static by.kilometry.kmdtests.acceptance.framework.util.WebElement.shouldBeInstantlyVisible;
import static org.hamcrest.MatcherAssert.assertThat;

public class PayViaBePaidService extends SerenityStory {
    @Steps
    Pages pages;

    @Given("payment page is displayed")
    @Composite(steps = {
            "Given home page is displayed",
            "When benefactor clicks 'Оплатить поездку' link on home page"
    })
    public void givenPaymentPageIsDisplayed() {
    }

    @Given("'bePaid' payment method is selected on payment page")
    public void givenBePaidPaymentMethodIsSelectedOnPaymentPage() {
        WebElementFacade defaultPaymentMethod = pages.payment.defaultMethod();
        assertThat(
                defaultPaymentMethod,
                equalTo("Онлайн оплата через сервис bePaid"));
    }

    @When("benefactor enters amount of payment on payment page")
    public void whenBenefactorEntersAmountOfPaymentOnPaymentPage() {
        int amount = Payment.defaultAmountInByn();
        pages.payment.amount().type(String.valueOf(amount));
        ScenarioSession.set(PAYMENT_AMOUNT, amount);
    }

    @When("benefactor selects 'Я согласен с условиями' checkbox")
    public void whenBenefactorSelectsIAmAgreedToConditionsCheckbox() {
        pages.payment.agreedCheckbox().click();
    }

    @When("benefactor clicks 'Оплатить' button on payment page")
    public void whenBenefactorClicksPayForButtonOnPaymentPage() {
        pages.payment.payForButton().click();
    }

    @Then("benefactor is redirected to bePaid website")
    public void thenBenefactorIsRedirectedToBePaidWebsite() {
        pages.bePaid.waitForCondition().
                until(hostIs(BePaid.host()));
    }

    @Given("benefactor is redirected to bePaid website")
    @Composite(steps = {
            "Given benefactor is registered on the site",
            "Given home page is displayed",
            "When user types his e-mail on home page",
            "When user types his password on home page",
            "When user clicks 'Войти' button on home page",
            "Then greeting is updated with user's name",
            "Then user icon button is updated with user's name",
            "Given home page is displayed",
            "When benefactor clicks 'Оплатить поездку' link on home page",
            "Given 'bePaid' payment method is selected on payment page",
            "When benefactor enters amount of payment on payment page",
            "When benefactor selects 'Я согласен с условиями' checkbox",
            "When benefactor clicks 'Оплатить' button on payment page",
            "Then benefactor is redirected to bePaid website"
    })
    public void givenBenefactorIsRedirectedToBePaidWebsite() {
    }

    @Given("benefactor has filled valid credit card details on bePaid website")
    public void givenBenefactorHasFilledValidCreditCardDetailsOnBePaidWebsite() {
        CreditCard card = fromPropertiesFile("creditcards/bepaid/success.properties");
        pages.bePaid.typeCreditCardNumber(card.number());
        pages.bePaid.validThruMonth().type(card.validThruMonth());
        pages.bePaid.validThruYear().type(card.validThruYear());
        pages.bePaid.cardholderName().type(card.cardholderName());
        pages.bePaid.cvv().type(card.cvv());
    }

    @Then("'$transactionResultMessage' message is displayed on bePaid website")
    public void thenTransactionResultMessageIsDisplayedOnBePaidWebsite(
            @Named("transactionResultMessage") String transactionResultMessage) {
        WebElementFacade actualMessage = pages.bePaid.transactionResultMessage();
        assertThat(actualMessage, equalTo(transactionResultMessage));
    }

    @Given("benefactor has filled invalid credit card details on bePaid website")
    public void givenBenefactorHasFilledInvalidCreditCardDetailsOnBePaidWebsite() {
        CreditCard card = fromPropertiesFile("creditcards/bepaid/fail.properties");
        pages.bePaid.typeCreditCardNumber(card.number());
        pages.bePaid.validThruMonth().type(card.validThruMonth());
        pages.bePaid.validThruYear().type(card.validThruYear());
        pages.bePaid.cardholderName().type(card.cardholderName());
        pages.bePaid.cvv().type(card.cvv());
    }

    @When("benefactor submits payment on bePaid website")
    public void whenBenefactorSubmitsPaymentOnBePaidWebsite() {
        pages.bePaid.payForButton().click();
    }

    @Then("'Продолжить' button is displayed on bePaid website")
    public void thenContinueButtonIsDisplayedOnBePaidWebsite() {
        shouldBeInstantlyVisible(pages.bePaid.continueButton());
    }

    @Given("payment via bePaid website has been finished successfully")
    @Composite(steps = {
            "Given benefactor is registered on the site",
            "Given home page is displayed",
            "When user types his e-mail on home page",
            "When user types his password on home page",
            "When user clicks 'Войти' button on home page",
            "Then greeting is updated with user's name",
            "Then user icon button is updated with user's name",
            "Given home page is displayed",
            "When benefactor clicks 'Оплатить поездку' link on home page",
            "Given 'bePaid' payment method is selected on payment page",
            "When benefactor enters amount of payment on payment page",
            "When benefactor selects 'Я согласен с условиями' checkbox",
            "When benefactor clicks 'Оплатить' button on payment page",
            "Then benefactor is redirected to bePaid website",
            "Given benefactor has filled valid credit card details on bePaid website",
            "When benefactor submits payment on bePaid website",
            "Then 'ТРАНЗАКЦИЯ ПРОШЛА УСПЕШНО' message is displayed on bePaid website",
            "Then 'Продолжить' button is displayed on bePaid website",
    })
    public void givenPaymentViaBePaidWebsiteHasBeenFinishedSuccessfully() {
    }

    @When("benefactor clicks 'Продолжить' button on bePaid website")
    public void whenBenefactorClicksContinueButtonOnBePaidWebsite() {
        pages.bePaid.continueButton().click();
    }

    @Then("benefactor is redirected to the website")
    public void thenBenefactorIsRedirectedToTheWebsite() {
        pages.paymentResult.waitForCondition().
                until(hostIs(PaymentResult.host()));
    }

    @Then("payment result page is displayed")
    public void thenPaymentResultPageIsDisplayed() {
        pages.paymentResult.waitForCondition().
                until(urlContains(PaymentResult.PATH));
    }

    @Then("amount of payment is shown on payment result page")
    public void thenAmountOfPaymentIsShownOnPaymentResultPage() {
        int expectedAmount = ScenarioSession.get(PAYMENT_AMOUNT, Integer.class);
        WebElementFacade actualAmount = pages.paymentResult.amount();
        assertThat(actualAmount, equalTo(expectedAmount));
    }

    @Then("payment method is shown on payment result page")
    public void thenPaymentMethodIsShownOnPaymentResultPage() {
        WebElementFacade actualMethod = pages.paymentResult.method();
        assertThat(actualMethod, equalTo("bePaid"));
    }

    @Then("status of the payment on payment result page is '$paymentStatus'")
    public void thenStatusOfThePaymentOnPaymentResultPageIsSuccess(
            @Named("paymentStatus") String paymentStatus) {
        WebElementFacade actualStatus = pages.paymentResult.status();
        assertThat(actualStatus, equalTo(paymentStatus));
    }

    @Then("'Перейти к списку платежей' link is shown on payment result page")
    public void thenGoToListOfPaymentsLinkIsShownOnPaymentResultPage() {
        shouldBeInstantlyVisible(pages.paymentResult.goToListOfPayments());
    }

    @Given("payment via bePaid website has been finished unsuccessfully")
    @Composite(steps = {
            "Given benefactor is registered on the site",
            "Given home page is displayed",
            "When user types his e-mail on home page",
            "When user types his password on home page",
            "When user clicks 'Войти' button on home page",
            "Then greeting is updated with user's name",
            "Then user icon button is updated with user's name",
            "Given home page is displayed",
            "When benefactor clicks 'Оплатить поездку' link on home page",
            "Given 'bePaid' payment method is selected on payment page",
            "When benefactor enters amount of payment on payment page",
            "When benefactor selects 'Я согласен с условиями' checkbox",
            "When benefactor clicks 'Оплатить' button on payment page",
            "Then benefactor is redirected to bePaid website",
            "Given benefactor has filled invalid credit card details on bePaid website",
            "When benefactor submits payment on bePaid website",
            "Then 'ТРАНЗАКЦИЯ БЫЛА ОТКЛОНЕНА' message is displayed on bePaid website",
            "Then 'Продолжить' button is displayed on bePaid website",
    })
    public void givenPaymentViaBePaidWebsiteHasBeenFinishedUnsuccessfully() {
    }
}
