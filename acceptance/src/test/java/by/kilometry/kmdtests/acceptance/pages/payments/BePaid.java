package by.kilometry.kmdtests.acceptance.pages.payments;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import static by.kilometry.kmdtests.acceptance.framework.util.PropertiesFileLoader.loadHostProperty;

public class BePaid extends PageObject {
    public static String host() {
        return loadHostProperty("bepaid.properties");
    }

    public void typeCreditCardNumber(String creditCardNumber) {
        String[] quarters = creditCardNumber.split(" ");
        CreditCardNumber inputField = new CreditCardNumber();
        inputField.firstQuarter().type(quarters[0]);
        inputField.secondQuarter().type(quarters[1]);
        inputField.thirdQuarter().type(quarters[2]);
        inputField.fourthQuarter().type(quarters[3]);
    }

    public WebElementFacade validThruMonth() {
        return find(By.id("request_credit_card_exp_month"));
    }

    public WebElementFacade validThruYear() {
        return find(By.id("request_credit_card_exp_year"));
    }

    public WebElementFacade cardholderName() {
        return find(By.id("request_credit_card_holder"));
    }

    public WebElementFacade cvv() {
        return find(By.id("request_credit_card_verification_value"));
    }

    public WebElementFacade payForButton() {
        return find(By.cssSelector("input[name='commit']"));
    }

    public WebElementFacade transactionResultMessage() {
        return find(By.xpath("//h1"));
    }

    public WebElementFacade continueButton() {
        return find(By.xpath(".//a[contains(text(),'Продолжить')]"));
    }

    private class CreditCardNumber {
        WebElementFacade firstQuarter() {
            return find(By.id("request_credit_card_number_1"));
        }

        WebElementFacade secondQuarter() {
            return find(By.id("request_credit_card_number_2"));
        }

        WebElementFacade thirdQuarter() {
            return find(By.id("request_credit_card_number_3"));
        }

        WebElementFacade fourthQuarter() {
            return find(By.id("request_credit_card_number_4"));
        }
    }
}
