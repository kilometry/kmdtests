package by.kilometry.kmdtests.acceptance.framework.util;

import by.kilometry.kmdtests.acceptance.framework.email.MailBox;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MailProviderTest {
    private static final Duration ONE_SECOND = Duration.ofSeconds(1);
    private static final Duration HALF_OF_SECOND = ONE_SECOND.dividedBy(2);

    @Nested
    @ExtendWith(MockitoExtension.class)
    class AnEmailReceived {
        @Mock
        private MailBox mailBox;

        @Test
        @DisplayName("Throws an exception when mail box wrapper is null.")
        void nullMailBox() {
            NullPointerException exception = assertThrows(
                    NullPointerException.class,
                    () -> MailProvider.anEmailReceived(null, 1, ONE_SECOND)
            );

            assertThat(exception.getMessage(), equalTo("Mail box wrapper should not be null."));
        }

        @Test
        @DisplayName("Throws an exception when maximum number of attempts is less than one.")
        void invalidNumberOfAttempts() {
            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> MailProvider.anEmailReceived(mailBox, 0, ONE_SECOND)
            );

            assertThat(
                    exception.getMessage(),
                    equalTo("Maximum number of attempts should be greater than zero but [0] was specified.")
            );
        }

        @Test
        @DisplayName("Throws an exception when pause between attempts is null.")
        void nullPauseBetweenAttempts() {
            NullPointerException exception = assertThrows(
                    NullPointerException.class,
                    () -> MailProvider.anEmailReceived(mailBox, 1, null)
            );

            assertThat(exception.getMessage(), equalTo("Pause between attempts should not be null."));
        }

        @Test
        @DisplayName("Throws an exception when pause between attemps is less than half of second.")
        void tooSmallPauseBetweenAttempts() {
            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> MailProvider.anEmailReceived(mailBox, 1, Duration.ofMillis(200))
            );

            assertThat(
                    exception.getMessage(),
                    equalTo("Pause between attempts should be at least half of second (e.g. 500 milliseconds) " +
                            "but [200] milliseconds was specified.")
            );
        }

        @Test
        @DisplayName("Returns false if maximum number of attempts has been exceeded.")
        void wasteAllTheAttempts() {
            when(mailBox.isEmpty()).thenReturn(true, true, true);

            boolean result = MailProvider.anEmailReceived(mailBox, 3, HALF_OF_SECOND);

            assertThat(result, is(false));
            verify(mailBox, times(3)).isEmpty();
        }

        @Test
        @DisplayName("Returns true if mail box became non-empty before attempts have been exhausted.")
        void happyPath() {
            when(mailBox.isEmpty()).thenReturn(true, false);

            boolean result = MailProvider.anEmailReceived(mailBox, 3, HALF_OF_SECOND);

            assertThat(result, is(true));
            verify(mailBox, times(2)).isEmpty();
        }
    }
}
