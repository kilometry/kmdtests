package by.kilometry.kmdtests.acceptance.stories.registration;

import by.kilometry.kmdtests.acceptance.framework.data.User;
import by.kilometry.kmdtests.acceptance.framework.email.MailBox;
import by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail.TempMail;
import by.kilometry.kmdtests.acceptance.framework.session.ScenarioSession;
import by.kilometry.kmdtests.acceptance.pages.Pages;
import by.kilometry.kmdtests.acceptance.pages.parts.RegistrationForm;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import java.time.Duration;
import java.util.function.Supplier;

import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text.Text.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.ACCOUNT_ACTIVATION_LINK;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.MAIL_BOX;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.USER;
import static by.kilometry.kmdtests.acceptance.framework.util.MailProvider.anEmailReceived;
import static by.kilometry.kmdtests.acceptance.framework.matcher.Url.urlEndsWith;
import static by.kilometry.kmdtests.acceptance.framework.util.WebElement.waitUntilReAttachedWithText;
import static com.jayway.awaitility.Awaitility.await;
import static com.jayway.awaitility.Duration.ONE_SECOND;
import static com.jayway.awaitility.Duration.TEN_SECONDS;
import static com.jayway.awaitility.Duration.TWO_HUNDRED_MILLISECONDS;
import static org.hamcrest.MatcherAssert.assertThat;

public class RegisterAsBenefactor extends SerenityStory {
    private static final Duration HALF_OF_SECOND = Duration.ofSeconds(1).dividedBy(2);

    @Steps
    Pages pages;

    @Given("registration form for benefactor is displayed")
    @Composite(steps = {
            "Given home page is displayed",
            "When user clicks 'как благотворитель' link on home page",
            "Then registration form is displayed",
            "Then benefactor radio button is selected by default on registration form"
    })
    public void givenRegistrationFormForBenefactorIsDisplayed() {
    }

    @When("benefactor fills all the mandatory fields on registration form")
    @Pending
    public void whenBenefactorFillsAllTheMandatoryFieldsOnRegistrationForm() {
        User benefactor = User.fromPropertiesFile("users/non-registered/benefactor.properties");
        RegistrationForm form = pages.home.registrationForm();
        form.inputField("Ваша фамилия").type(benefactor.lastname());
        form.inputField("Ваше имя").type(benefactor.firstname());
        form.inputField("Ваше отчество").type(benefactor.middlename());
        MailBox mailBox = TempMail.newBox();
        benefactor.email(mailBox.address());
        form.inputField("Email, по которому с вами можно связаться").type(benefactor.email());
        form.inputField("Номер телефона").type(benefactor.phone());
        form.inputField("Пароль").type(benefactor.password());
        form.inputField("Повторите пароль").type(benefactor.password());
        ScenarioSession.set(USER, benefactor);
        ScenarioSession.set(MAIL_BOX, mailBox);
    }

    @When("benefactor submits registration form")
    @Alias("family submits registration form")
    public void whenBenefactorSubmitsRegistrationForm() {
        pages.home.registrationForm().
                submitButton().
                click();
    }

    @Then("'$registrationIsComplete' message is displayed")
    public void thenRegistrationIsCompletedMessageIsDisplayed(
            @Named("registrationIsComplete") String registrationIsComplete) {
        Supplier<WebElementFacade> message = () -> pages.home.message();
        waitUntilReAttachedWithText(message, registrationIsComplete);
        assertThat(message.get(), equalTo(registrationIsComplete));
    }

    @Then("e-mail with a link for account activation is sent")
    public void thenEmailWithALinkForAccountActivationIsSent() {
        MailBox mailBox = ScenarioSession.get(MAIL_BOX, MailBox.class);
        await("receive an e-mail with a link for account activation").
                pollDelay(ONE_SECOND).
                pollInterval(TWO_HUNDRED_MILLISECONDS).
                atMost(TEN_SECONDS).
                until(() -> anEmailReceived(mailBox, 5, HALF_OF_SECOND));
        ScenarioSession.set(ACCOUNT_ACTIVATION_LINK, mailBox.getAccountActivationLink());
    }

    @Given("benefactor has received a link for account activation")
    @Alias("family has received a link for account activation")
    @Composite(steps = {
            "Given registration form for benefactor is displayed",
            "When benefactor fills all the mandatory fields on registration form",
            "When benefactor submits registration form",
            "Then 'Регистрация завершена' message is displayed",
            "Then e-mail with a link for account activation is sent"
    })
    public void givenBenefactorHasReceivedALinkForAccountActivation() {
    }

    @When("benefactor follows the link for account activation")
    @Alias("family follows the link for account activation")
    public void whenBenefactorFollowsTheLinkForAccountActivation() {
        String link = ScenarioSession.get(ACCOUNT_ACTIVATION_LINK, String.class);
        pages.accountActivation.openAt(link);
    }

    @Then("e-mail confirmed page is displayed")
    public void thenEmailConfirmedPageIsDisplayed() {
        pages.accountActivation.waitForCondition().
                until(urlEndsWith("/email-confirmed"));
    }

    @Then("e-mail confirmed page contains '$accountIsActivated' message")
    public void thenEmailConfirmedPageContainsAccountIsActivatedMessage(
            @Named("accountIsActivated") String accountIsActivated) {
        WebElementFacade message = pages.accountActivation.message();
        assertThat(message, equalTo(accountIsActivated));
    }
}
