package by.kilometry.kmdtests.acceptance.pages.payments;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import static by.kilometry.kmdtests.acceptance.framework.util.PropertiesFileLoader.loadHostProperty;

public class PaymentResult extends PageObject {
    public static String host() {
        return loadHostProperty("site.properties");
    }

    public static final String PATH = "#/payment-result";

    public WebElementFacade amount() {
        return find(By.xpath("//span[contains(text(),'Сумма:')]")).
                find(By.xpath("../span[2]"));
    }

    public WebElementFacade method() {
        return find(By.xpath("//span[contains(text(),'Способ оплаты:')]")).
                find(By.xpath("../span[2]"));
    }

    public WebElementFacade status() {
        return find(By.xpath("//span[contains(text(),'Статус:')]")).
                find(By.xpath("../span[2]"));
    }

    public WebElementFacade goToListOfPayments() {
        return find(By.xpath("//a[contains(text(),'Перейти к списку платежей')]"));
    }
}
