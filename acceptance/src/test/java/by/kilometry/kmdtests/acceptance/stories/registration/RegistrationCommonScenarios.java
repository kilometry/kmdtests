package by.kilometry.kmdtests.acceptance.stories.registration;

import by.kilometry.kmdtests.acceptance.framework.util.WebElement;
import by.kilometry.kmdtests.acceptance.pages.Pages;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import java.util.List;

import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.control.RadioButton.checked;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text.Texts.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.util.NamedParameters.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class RegistrationCommonScenarios {
    @Steps
    Pages pages;

    @When("user clicks 'Регистрация' button")
    public void whenUserClicksRegistrationButton() {
        pages.home.userIconButton().
                registration().
                click();
    }

    @Then("registration form is displayed")
    public void thenRegistrationFormIsDisplayed() {
        WebElementFacade form = pages.home.registrationForm().root();
        WebElement.shouldBeInstantlyVisible(form);
    }

    @Then("the radio buttons on registration form are: $radioButtonsTable")
    public void thenTheRadioButtonsOnRegistrationFormAre(ExamplesTable radioButtonsTable) {
        List<WebElementFacade> buttonLabels = pages.home.registrationForm().
                radioButtonLabels();
        List<String> expectedLabels = asList(radioButtonsTable, "Name");
        assertThat(buttonLabels, equalTo(expectedLabels));
    }

    @Then("benefactor radio button is selected by default on registration form")
    public void thenBenefactorRadioButtonIsSelectedByDefaultOnRegistrationForm() {
        WebElementFacade button = pages.home.registrationForm().
                benefactorRadioButton();
        assertThat(button, is(checked()));
    }

    @Then("family radio button is selected by default on registration form")
    public void thenFamilyRadioButtonIsSelectedByDefaultOnRegistrationForm() {
        WebElementFacade button = pages.home.registrationForm().
                familyRadioButton();
        assertThat(button, is(checked()));
    }


    @Then("the input fields on registration form are: $fieldsTable")
    public void thenTheInputFieldsOnRegistrationFormAre(ExamplesTable fieldsTable) {
        List<WebElementFacade> actualFields = pages.home.registrationForm().
                inputFieldLabels();
        List<String> expectedFields = asList(fieldsTable, "Name");
        assertThat(actualFields, equalTo(expectedFields));
    }

    @Then("the buttons on registration form are: $buttonsTable")
    public void thenTheButtonsOnRegistrationFormAre(ExamplesTable buttonsTable) {
        pages.home.registrationForm().
                buttons(asList(buttonsTable, "Name")).
                forEach(WebElement::shouldBeInstantlyVisible);
    }

    @When("user clicks '$userType' link on home page")
    public void whenUserClicksUserTypeLinkOnHomePage(@Named("userType") String userType) {
        pages.home.loginForm().
                link(userType).
                click();
    }
}
