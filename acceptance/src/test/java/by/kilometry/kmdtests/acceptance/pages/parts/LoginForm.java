package by.kilometry.kmdtests.acceptance.pages.parts;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

public class LoginForm extends Part {
    public LoginForm(WebElementFacade root) {
        super(root);
    }

    public WebElementFacade emailField() {
        return root.find(By.id("email"));
    }

    public WebElementFacade passwordField() {
        return root.find(By.id("password"));
    }

    public WebElementFacade submitButton() {
        return root.findBy(By.id("submit"));
    }

    public WebElementFacade alert() {
        return root.findBy(By.cssSelector("div.alert"));
    }

    public WebElementFacade greeting() {
        return root.find(By.cssSelector("h2"));
    }

    public WebElementFacade link(String text) {
        return childButtonWithText(text);
    }
}