package by.kilometry.kmdtests.acceptance.stories.payment;

import by.kilometry.kmdtests.acceptance.framework.data.User;
import by.kilometry.kmdtests.acceptance.framework.session.ScenarioSession;
import by.kilometry.kmdtests.acceptance.pages.Pages;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static by.kilometry.kmdtests.acceptance.framework.data.User.fromPropertiesFile;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.control.Checkbox.selected;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text.Text.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.matcher.Url.urlEndsWith;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.kilometres.BalanceExact.balanceFromWebElement;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.kilometres.BalanceExact.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.kilometres.BalanceIncreased.BalanceIncreasedMatcherBuilder.increasedFrom;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.BALANCE;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.PAYMENT_AMOUNT;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.USER;
import static by.kilometry.kmdtests.acceptance.framework.util.CurrencyExchange.bynToKilometer;
import static by.kilometry.kmdtests.acceptance.framework.util.WebElement.shouldBeInstantlyVisible;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PaymentCommonScenarios extends SerenityStory {
    @Steps
    Pages pages;

    @Given("benefactor is registered on the site")
    public void givenBenefactorIsRegisteredOnTheSite() {
        User benefactor = fromPropertiesFile("users/registered/activated/benefactor.properties");
        ScenarioSession.set(USER, benefactor);
    }

    @Given("benefactor is logged in")
    @Composite(steps = {
            "Given home page is displayed",
            "When user types his e-mail on home page",
            "When user types his password on home page",
            "When user clicks 'Войти' button on home page",
            "Then greeting is updated with user's name",
            "Then user icon button is updated with user's name"
    })
    public void givenBenefactorIsLoggedIn() {
    }

    @When("benefactor clicks 'Оплатить поездку' link on home page")
    public void whenBenefactorClicksPayForJourneyLinkOnHomePage() {
        pages.home.payForJourneyLink().click();
    }

    @Then("payment page is displayed")
    public void thenPaymentPageIsDisplayed() {
        pages.payment.waitForCondition().
                until(urlEndsWith("#/ride-payment"));
    }

    @Then("payment page contains drop-down for payment method")
    public void thenPaymentPageContainsDropdownForPaymentMethod() {
        shouldBeInstantlyVisible(pages.payment.methodDropdown());
    }

    @Then("'bePaid' payment method is selected by default")
    public void thenBePaidPaymentMethodIsSelectedByDefault() {
        WebElementFacade defaultPaymentMethod = pages.payment.defaultMethod();
        assertThat(
                defaultPaymentMethod,
                equalTo("Онлайн оплата через сервис bePaid"));
    }

    @Then("payment page contains input field for payment amount")
    public void thenPaymentPageContainsInputFieldForPaymentAmount() {
        shouldBeInstantlyVisible(pages.payment.amount());
    }

    @Then("payment page contains 'Я согласен с условиями' checkbox")
    public void thenPaymentPageContainsIAmAgreedToConditionsCheckbox() {
        shouldBeInstantlyVisible(pages.payment.agreedCheckbox());
    }

    @Then("'Я согласен с условиями' checkbox is not selected by default")
    public void thenIAmAgreedToConditionsCheckboxIsNotSelectedByDefault() {
        assertThat(
                pages.payment.agreedCheckbox(),
                is(not(selected())));
    }

    @Then("payment page contains 'Оплатить' button")
    public void thenPaymentPageContainsPayForButton() {
        shouldBeInstantlyVisible(pages.payment.payForButton());
    }

    @Given("profile page for benefactor is displayed")
    public void givenProfilePageForBenefactorIsDisplayed() {
        pages.home.userIconButton().root().click();
        pages.home.userIconButton().profile().click();
        pages.profile.waitForCondition().
                until(urlEndsWith("#/profile/payments"));
    }

    @Given("benefactor sees balance of kilometers in profile")
    public void givenBenefactorSeesBalanceOfKilometersInProfile() {
        WebElementFacade balanceElement = pages.profile.balance();
        shouldBeInstantlyVisible(balanceElement);
        int balanceValue = balanceFromWebElement(balanceElement);
        ScenarioSession.set(BALANCE, balanceValue);
    }

    @When("benefactor makes successful payment")
    @Composite(steps = {
            "Given home page is displayed",
            "When benefactor clicks 'Оплатить поездку' link on home page",
            "Given 'bePaid' payment method is selected on payment page",
            "When benefactor enters amount of payment on payment page",
            "When benefactor selects 'Я согласен с условиями' checkbox",
            "When benefactor clicks 'Оплатить' button on payment page",
            "Then benefactor is redirected to bePaid website",
            "Given benefactor has filled valid credit card details on bePaid website",
            "When benefactor submits payment on bePaid website",
            "Then 'ТРАНЗАКЦИЯ ПРОШЛА УСПЕШНО' message is displayed on bePaid website",
            "Then 'Продолжить' button is displayed on bePaid website",
            "When benefactor clicks 'Продолжить' button on bePaid website",
            "Then benefactor is redirected to the website",
            "Then payment result page is displayed"
    })
    public void whenBenefactorMakesSuccessfulPayment() {
    }

    @When("benefactor opens profile page again")
    public void whenBenefactorOpensProfilePageAgain() {
        pages.home.userIconButton().root().click();
        pages.home.userIconButton().profile().click();
        pages.profile.waitForCondition().
                until(urlEndsWith("#/profile/payments"));
    }

    @Then("balance of kilometers is increased proportionally to amount of the payment")
    public void thenBalanceOfKilometersIsIncreasedProportionallyToAmountOfThePayment() {
        int oldBalance = ScenarioSession.get(BALANCE, Integer.class);
        int paymentAmount = ScenarioSession.get(PAYMENT_AMOUNT, Integer.class);
        int expectedRise = bynToKilometer(paymentAmount);
        assertThat(
                pages.profile.balance(),
                increasedFrom(oldBalance).by(expectedRise));
    }

    @When("benefactor makes failed payment")
    @Composite(steps = {
            "Given home page is displayed",
            "When benefactor clicks 'Оплатить поездку' link on home page",
            "Given 'bePaid' payment method is selected on payment page",
            "When benefactor enters amount of payment on payment page",
            "When benefactor selects 'Я согласен с условиями' checkbox",
            "When benefactor clicks 'Оплатить' button on payment page",
            "Then benefactor is redirected to bePaid website",
            "Given benefactor has filled invalid credit card details on bePaid website",
            "When benefactor submits payment on bePaid website",
            "Then 'ТРАНЗАКЦИЯ БЫЛА ОТКЛОНЕНА' message is displayed on bePaid website",
            "Then 'Продолжить' button is displayed on bePaid website",
            "When benefactor clicks 'Продолжить' button on bePaid website",
            "Then benefactor is redirected to the website",
            "Then payment result page is displayed"
    })
    public void whenBenefactorMakesFailedPayment() {
    }

    @Then("balance of kilometers is left unchanged in profile")
    public void thenBalanceOfKilometersIsLeftUnchangedInProfile() {
        int oldBalance = ScenarioSession.get(BALANCE, Integer.class);
        WebElementFacade newBalance = pages.profile.balance();
        assertThat(newBalance, equalTo(oldBalance));
    }
}
