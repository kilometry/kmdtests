package by.kilometry.kmdtests.acceptance.framework.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ParsingTest {
    @Nested
    class Kilometres {
        @Test
        @DisplayName("Throws exception when input string is null.")
        void inputStringIsNull() {
            NullPointerException exception = assertThrows(
                    NullPointerException.class,
                    () -> Parsing.kilometres(null));

            assertThat(
                    exception.getMessage(),
                    equalTo("Input string should not be null."));
        }

        @Test
        @DisplayName("Throws exception when input string is empty.")
        void inputStringIsEmpty() {
            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> Parsing.kilometres(""));

            assertThat(
                    exception.getMessage(),
                    equalTo("Input string should not be empty."));
        }

        @Test
        @DisplayName("Throws exception when input string is malformed")
        void inputStringIsMalformed() {
            String malformedBalance = "5километров";

            IllegalStateException exception = assertThrows(
                    IllegalStateException.class,
                    () -> Parsing.kilometres(malformedBalance));

            assertThat(
                    exception.getMessage(),
                    equalTo("Failed to parse string [5километров] using regular " +
                            "expression [(\\d+) км\\.]."));
        }

        @Test
        @DisplayName("Returns amount of kilometers as number")
        void happyPath() {
            String balance = "120 км.";

            assertThat(Parsing.kilometres(balance), is(120));
        }
    }

    @Nested
    class Byn {
        @Test
        @DisplayName("Throws exception when input string is null.")
        void inputStringIsNull() {
            NullPointerException exception = assertThrows(
                    NullPointerException.class,
                    () -> Parsing.byn(null));

            assertThat(
                    exception.getMessage(),
                    equalTo("Input string should not be null."));
        }

        @Test
        @DisplayName("Throws exception when input string is empty.")
        void inputStringIsEmpty() {
            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> Parsing.byn(""));

            assertThat(
                    exception.getMessage(),
                    equalTo("Input string should not be empty."));
        }

        @Test
        @DisplayName("Throws exception when input string is malformed")
        void inputStringIsMalformed() {
            String malformedAmount = "2 BYR";

            IllegalStateException exception = assertThrows(
                    IllegalStateException.class,
                    () -> Parsing.byn(malformedAmount));

            assertThat(
                    exception.getMessage(),
                    equalTo("Failed to parse string [2 BYR] using regular " +
                            "expression [(\\d+) BYN]."));
        }

        @Test
        @DisplayName("Returns amount of rubles as number")
        void happyPath() {
            String balance = "5 BYN";

            assertThat(Parsing.byn(balance), is(5));
        }
    }
}
