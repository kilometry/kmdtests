package by.kilometry.kmdtests.acceptance.stories.logout;

import by.kilometry.kmdtests.acceptance.pages.Pages;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static by.kilometry.kmdtests.acceptance.framework.util.WebElement.shouldBeInstantlyVisible;

public class LogOut extends SerenityStory {
    @Steps
    Pages pages;

    @Composite(steps = {
            "Given user is registered on the site",
            "Given home page is displayed",
            "When user types his e-mail on home page",
            "When user types his password on home page",
            "When user clicks 'Войти' button on home page",
            "Then greeting is updated with user's name",
            "Then user icon button is updated with user's name"
    })
    @Given("user is logged in")
    public void givenUserIsLoggedIn() {
    }

    @When("user clicks 'Выйти' button")
    public void whenUserClicksLogoutButton() {
        pages.home.userIconButton().
                logout().
                click();
    }

    @Then("home page is displayed")
    public void thenHomePageIsDisplayed() {
        pages.home.shouldBeDisplayed();
    }

    @Then("a field for entering e-mail is appeared on home page")
    public void thenAFieldForEnteringEmailIsAppearedOnHomePage() {
        WebElementFacade field = pages.home.loginForm().
                emailField();
        shouldBeInstantlyVisible(field);

    }

    @Then("a field for entering password is appeared on home page")
    @Pending
    public void thenAFieldForEnteringPasswordIsAppearedOnHomePage() {
        WebElementFacade field = pages.home.loginForm().
                passwordField();
        shouldBeInstantlyVisible(field);
    }

    @Then("'Войти' button is appeared on home page")
    @Pending
    public void thenLoginButtonIsAppearedOnHomePage() {
        WebElementFacade button = pages.home.loginForm().
                submitButton();
        shouldBeInstantlyVisible(button);
    }
}
