package by.kilometry.kmdtests.acceptance.pages.parts;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;
import java.util.stream.Stream;

import static by.kilometry.kmdtests.acceptance.framework.util.StreamApi.toUnmodifiableList;
import static java.lang.String.format;
import static java.util.Collections.singletonList;

public class RegistrationForm extends Part {
    public RegistrationForm(WebElementFacade root) {
        super(root);
    }

    public WebElementFacade benefactorRadioButton() {
        return root.find(By.id("reg-benefactor"));
    }

    public WebElementFacade familyRadioButton() {
        return root.find(By.id("reg-family"));
    }

    public List<WebElementFacade> radioButtonLabels() {
        return Stream.<WebElementFacade>of(
                benefactorRadioButton(),
                familyRadioButton()).
                map(RegistrationForm::labelOf).
                collect(toUnmodifiableList());
    }

    public List<WebElementFacade> inputFieldLabels() {
        return root.thenFindAll(By.cssSelector("input.form-control")).
                stream().
                map(RegistrationForm::labelOf).
                collect(toUnmodifiableList());
    }

    public WebElementFacade inputField(String name) {
        return root.thenFindAll(By.cssSelector("input.form-control")).
                stream().
                map(RegistrationForm::labelOf).
                filter(label -> label.getText().contains(name)).
                map(RegistrationForm::inputOf).
                findFirst().
                orElseThrow(() -> {
                    String message = format("Cannot find input field by label [%s].", name);
                    return new IllegalStateException(message);
                });
    }

    public List<WebElementFacade> buttons(List<String> names) {
        return names.stream().
                map(this::childButtonWithText).
                collect(toUnmodifiableList());
    }

    public WebElementFacade submitButton() {
        return buttons(singletonList("Зарегистрироваться")).
                iterator().
                next();
    }

    private static WebElementFacade labelOf(WebElementFacade input) {
        return input.find(By.xpath("./..")).
                then(By.cssSelector("label"));
    }

    private static WebElementFacade inputOf(WebElementFacade label) {
        return label.find(By.xpath("./..")).
                then(By.cssSelector("input"));
    }
}