package by.kilometry.kmdtests.acceptance.stories.login;

import by.kilometry.kmdtests.acceptance.framework.data.User;
import by.kilometry.kmdtests.acceptance.framework.session.ScenarioSession;
import by.kilometry.kmdtests.acceptance.framework.util.WebElement;
import by.kilometry.kmdtests.acceptance.pages.Pages;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import static by.kilometry.kmdtests.acceptance.framework.data.User.fromPropertiesFile;
import static by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text.Text.equalTo;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.USER;
import static by.kilometry.kmdtests.acceptance.framework.util.NamedParameters.asList;
import static org.hamcrest.MatcherAssert.assertThat;

public class LogIn extends SerenityStory {
    private static final String NON_REGISTERED_EMAIL = "this_email_is_not_registered@mail.com";
    private static final String INVALID_PASSWORD = "this_password_is_invalid";

    private static User userFromSession() {
        return ScenarioSession.get(USER, User.class);
    }

    @Steps
    Pages pages;

    @Given("home page is displayed")
    public void givenHomePageIsDisplayed() {
        pages.home.open();
    }

    @When("user clicks on user icon button")
    public void whenUserClicksOnUserIconButton() {
        pages.home.userIconButton().
                root().
                click();
    }

    @When("user clicks 'Вход' button")
    public void whenUserClicksLoginButton() {
        pages.home.userIconButton().
                login().
                click();
    }

    @Then("separate login form is displayed")
    public void thenSeparateLoginFormIsDisplayed() {
        WebElementFacade form = pages.home.separateLoginForm().
                root();
        WebElement.shouldBeInstantlyVisible(form);
    }

    @Then("the input fields on login form are: $fieldsTable")
    public void thenTheInputFieldsOnLoginFormAre(ExamplesTable fieldsTable) {
        pages.home.separateLoginForm().
                inputFields(asList(fieldsTable, "Name")).
                forEach(WebElement::shouldBeInstantlyVisible);
    }

    @Then("the buttons on login form are: $buttonsTable")
    public void thenTheButtonsOnLoginFormAre(ExamplesTable buttonsTable) {
        pages.home.separateLoginForm().
                buttons(asList(buttonsTable, "Name")).
                forEach(WebElement::shouldBeInstantlyVisible);
    }

    @Then("the links on login form are: $linksTable")
    public void thenTheLinksOnTheFormAre(ExamplesTable linksTable) {
        pages.home.separateLoginForm().
                registrationLinks(asList(linksTable, "Text")).
                forEach(WebElement::shouldBeInstantlyVisible);
    }

    @Given("user is registered on the site")
    public void givenUserIsRegisteredOnTheSite() {
        // Pure test should work "from scratch". E.g. new userFromSession should be registered before each
        // execution of the scenario. But registration process is quite long and complex (due to
        // mandatory verification of new account via link sent by e-mail).
        //
        // From other side, non-repeatable userFromSession accounts (more precisely, e-mails) can look very
        // cryptic in case of test failure.
        //
        // So it will be better to re-use the same account in each scenario. This is why this
        // step is empty; we just load sample userFromSession data from the file.
        User user = fromPropertiesFile("users/registered/activated/user.properties");
        ScenarioSession.set(USER, user);
    }

    @Given("separate login form is displayed")
    @Composite(steps = {
            "Given home page is displayed",
            "When user clicks on user icon button",
            "When user clicks 'Вход' button",
            "Then separate login form is displayed"
    })
    public void givenSeparateLoginFormIsDisplayed() {
    }

    @When("user types his e-mail on login form")
    public void whenUserTypesHisEmailOnLoginForm() {
        String email = userFromSession().email();
        pages.home.separateLoginForm().
                inputField("Email").
                type(email);
    }

    @When("user types his password on login form")
    public void whenUserTypesHisPasswordOnLoginForm() {
        String password = userFromSession().password();
        pages.home.separateLoginForm().
                inputField("Пароль").
                type(password);
    }

    @When("user submits login form")
    public void whenUserSubmitsLoginForm() {
        pages.home.separateLoginForm().
                button("Войти").
                click();
    }

    @Then("greeting is updated with user's name")
    public void thenGreetingIsUpdatedWithUsersName() {
        String expectedGreeting = "Добро пожаловать, " +
                userFromSession().firstname() +
                ".";
        WebElementFacade actualGreeting = pages.home.loginForm().
                greeting();
        assertThat(actualGreeting, equalTo(expectedGreeting));
    }

    @Then("user icon button is updated with user's name")
    public void thenUserIconButtonIsUpdatedWithUsersName() {
        String expectedName = userFromSession().firstname();
        WebElementFacade actualName = pages.home.userIconButton().
                userName();
        assertThat(actualName, equalTo(expectedName));
    }

    @When("user types his e-mail on home page")
    public void whenUserTypesHisEmailOnHomePage() {
        String email = userFromSession().email();
        pages.home.loginForm().
                emailField().
                type(email);
    }

    @When("user types his password on home page")
    public void whenUserTypesHisPasswordOnHomePage() {
        String password = userFromSession().password();
        pages.home.loginForm().
                passwordField().
                type(password);
    }

    @When("user clicks 'Войти' button on home page")
    public void whenUserClicksLoginButtonOnHomePage() {
        pages.home.loginForm().
                submitButton().
                click();
    }

    @When("user types non-registered e-mail on home page")
    public void whenUserTypesNonRegisteredEmailOnHomePage() {
        pages.home.loginForm().
                emailField().
                type(NON_REGISTERED_EMAIL);
    }

    @When("user types invalid password on home page")
    public void whenUserTypesInvalidPasswordOnHomePage() {
        pages.home.loginForm().
                passwordField().
                type(INVALID_PASSWORD);
    }


    @Then("'$alertText' alert is appeared")
    public void thenMessageIsAppeared(@Named("alertText") String alertText) {
        WebElementFacade alert = pages.home.loginForm().
                alert();
        assertThat(alert, equalTo(alertText));
    }

    @Given("user had registered an account but had not activated it")
    public void givenUserHadRegisteredAnAccountButHadNotActivatedIt() {
        // Similar situation with prepared test data as for `Given user is registered on the site' case.
        User user = fromPropertiesFile("users/registered/non-activated/user.properties");
        ScenarioSession.set(USER, user);
    }
}