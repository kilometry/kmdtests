/**
 * Representation of HTML-pages on the website.
 * <p>
 * Classes in this package are designed in accordance to <strong>Page Object</strong> pattern. However, pages consist of
 * more-or-less independent blocks. So little classes that represent such parts are placed into separate package.
 * <p>
 * The last but not least issue are selectors. As can be seen, a lot of CSS and XPath selectors rely on presence of
 * certain text strings in web-elements. It's not good practice in general. But there is no better way right now:
 * elements in HTML-pages lack of strict IDs conventions.
 */
package by.kilometry.kmdtests.acceptance.pages;