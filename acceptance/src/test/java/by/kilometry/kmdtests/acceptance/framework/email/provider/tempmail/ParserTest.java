package by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail;

import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testing parser isn't beneficial work because it's impossible to cover each case (and it's dull work even with aid of
 * parameterized tests). So in this test we concentrate only on the most important cases.
 */
class ParserTest {
    @Nested
    class EmailAddressFromHtmlPage {
        @DisplayName("Throws an exception with the page source is empty.")
        @Test
        void pageIsEmpty() {
            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> Parser.emailAddressFromHtmlPage("")
            );

            assertThat(
                    exception.getMessage(),
                    equalTo("Source of HTML pages should not be empty.")
            );
        }

        @DisplayName("Throws an exception when there is no INPUT tag in page source")
        @Test
        void noInputTagWithAddress() {
            IllegalStateException exception = assertThrows(
                    IllegalStateException.class,
                    () -> Parser.emailAddressFromHtmlPage("Source of HTML page.")
            );

            assertThat(
                    exception.getMessage(),
                    allOf(
                            containsString("Failed to extract e-mail address from HTML page"),
                            containsString("Source of HTML page.")
                    )
            );
        }

        @DisplayName("Throws an exception when INPUT tag is presented but it doesn't contain an e-mail address")
        @Test
        void noEmailAddress() {
            IllegalStateException exception = assertThrows(
                    IllegalStateException.class,
                    () -> Parser.emailAddressFromHtmlPage("<input id=\"mail\"  " +
                            "data-original-title=\"Your temporary Email address\" " +
                            " value=\"not_an_email_address\" >")
            );

            assertThat(
                    exception.getMessage(),
                    allOf(
                            containsString("Failed to extract e-mail address from HTML page"),
                            containsString("not_an_email_address")
                    )
            );
        }

        @DisplayName("Returns an e-mail address.")
        @Test
        void happyPath() {
            String address = Parser.emailAddressFromHtmlPage("<input id=\"mail\"  " +
                    "data-original-title=\"Your temporary Email address\" " +
                    " value=\"mail@mail.com\" >");

            assertThat(address, equalTo("mail@mail.com"));
        }
    }

    @Nested
    class AccountActivationLinkFromEmail {
        @DisplayName("Throws an exception when email is empty.")
        @Test
        void emptyEmail() {
            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> Parser.accountActivationLinkFromEmail("")
            );

            assertThat(
                    exception.getMessage(),
                    equalTo("Text of e-mail should not be empty.")
            );
        }

        @DisplayName("Throws an exception when email doesn't contain a link.")
        @Test
        void noLink() {
            IllegalStateException exception = assertThrows(
                    IllegalStateException.class,
                    () -> Parser.accountActivationLinkFromEmail("Email text is here.")
            );

            assertThat(
                    exception.getMessage(),
                    allOf(
                            containsString("Cannot extract link for account activation"),
                            containsString("Email text is here.")
                    )
            );
        }

        @DisplayName("Returns link for account activation.")
        @Test
        void happyPath() {
            String link = Parser.accountActivationLinkFromEmail("Account confirmation link: " +
                    "<a href=\"http://kmdsite.com/account/activate\">link</a>");

            assertThat(link, equalTo("http://kmdsite.com/account/activate"));
        }
    }

    @Nested
    class TextFromEmail {
        @DisplayName("Throws an exception when JSON doesn't contain desired key.")
        @Test
        void noKeyInJson() {
            final JSONObject json = new JSONObject().
                    put("id", "some id");

            IllegalArgumentException exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> Parser.textFromEmail(json)
            );

            assertThat(
                    exception.getMessage(),
                    equalTo("E-mail JSON [{\"id\": \"some id\"}] " +
                            "doesn't contain [mail_text_only] key.")
            );
        }

        @DisplayName("Returns text of e-mail.")
        @Test
        void happyPath() {
            final JSONObject json = new JSONObject().
                    put("id", "some id").
                    put("mail_text_only", "This is e-mail text.");

            String emailText = Parser.textFromEmail(json);

            assertThat(emailText, equalTo("This is e-mail text."));
        }
    }
}
