package by.kilometry.kmdtests.acceptance.pages;

import by.kilometry.kmdtests.acceptance.pages.payments.BePaid;
import by.kilometry.kmdtests.acceptance.pages.payments.Payment;
import by.kilometry.kmdtests.acceptance.pages.payments.PaymentResult;

/**
 * Serenity BDD is universal testing framework. It's useful not only with combination with Selenium and JBehave but with
 * pure JUnit as well. Perhaps, this is why Serenity provides concept of its own "Steps".
 * <p>
 * As for our Acceptance Tests, we don't need Serenity Steps because the steps are implemented using JBehave. But
 * Serenity Steps is the only way to make web-driver working (and generate test reports). So we cannot avoid usage of
 * {@link net.thucydides.core.annotations.Steps} annotation and have to mimic in order to satisfy Serenity and keep the
 * source code readable. We should put the following field in test class:
 * <pre>
 *     {@literal @}Steps
 *     Pages pages;
 * </pre>
 * <p>
 * As a side effect, this class represents a handy list of all the pages on the site. And there is no need to put dull
 * getters here because class' fields will be implicitly managed by Serenity BDD.
 */
public class Pages {
    public Home home;
    public AccountActivation accountActivation;
    public Payment payment;
    public BePaid bePaid;
    public PaymentResult paymentResult;
    public Profile profile;
}