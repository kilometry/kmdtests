package by.kilometry.kmdtests.acceptance.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class AccountActivation extends PageObject {
    public WebElementFacade message() {
        return find(By.cssSelector("h1.h1-text"));
    }
}
