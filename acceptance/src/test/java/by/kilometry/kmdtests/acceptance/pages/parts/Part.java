package by.kilometry.kmdtests.acceptance.pages.parts;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

import static java.lang.String.format;

abstract class Part {
    final WebElementFacade root;

    Part(WebElementFacade root) {
        this.root = root;
    }

    public WebElementFacade root() {
        return root;
    }

    WebElementFacade childButtonWithText(String text) {
        String expression = format(".//button[contains(text(),'%s')]", text);
        return root.findBy(By.xpath(expression));
    }
}
