package by.kilometry.kmdtests.acceptance;

import net.serenitybdd.jbehave.SerenityStories;

/**
 * Maven-trigger for Acceptance Tests.
 * <p>
 * Serenity BDD should be ran at <i>integration-test</i> phase (in order to generate test reports). The name of this
 * class ends with <strong>IT</strong> suffix. So Maven recognizes it as Integration Test and delegates the reins of
 * government to Serenity BDD.
 */
public class SerenityIT extends SerenityStories {
}
