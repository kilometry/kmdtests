package by.kilometry.kmdtests.acceptance.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class Profile extends PageObject {
    public WebElementFacade balance() {
        return find(By.xpath("//div[contains(text(),'Ваш баланс:')]")).
                find(By.xpath(".//span"));
    }
}
