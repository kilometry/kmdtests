package by.kilometry.kmdtests.acceptance.stories.registration;

import by.kilometry.kmdtests.acceptance.framework.data.User;
import by.kilometry.kmdtests.acceptance.framework.email.MailBox;
import by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail.TempMail;
import by.kilometry.kmdtests.acceptance.framework.session.ScenarioSession;
import by.kilometry.kmdtests.acceptance.pages.Pages;
import by.kilometry.kmdtests.acceptance.pages.parts.RegistrationForm;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;

import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.MAIL_BOX;
import static by.kilometry.kmdtests.acceptance.framework.session.SessionKey.USER;

public class RegisterAsFamily extends SerenityStory {
    @Steps
    Pages pages;

    @Given("registration form for family is displayed")
    @Composite(steps = {
            "Given home page is displayed",
            "When user clicks 'как семья с особенным ребенком' link on home page",
            "Then registration form is displayed",
            "Then family radio button is selected by default on registration form"
    })
    public void givenRegistrationFormForFamilyIsDisplayed() {
    }

    @When("family fills all the mandatory fields on registration form")
    public void whenFamilyFillsAllTheMandatoryFieldsOnRegistrationForm() {
        User family = User.fromPropertiesFile("users/non-registered/family.properties");
        RegistrationForm form = pages.home.registrationForm();
        form.inputField("Ваша фамилия").type(family.lastname());
        form.inputField("Ваше имя").type(family.firstname());
        form.inputField("Ваше отчество").type(family.middlename());
        MailBox mailBox = TempMail.newBox();
        family.email(mailBox.address());
        form.inputField("Email, по которому с вами можно связаться").type(family.email());
        form.inputField("Номер телефона").type(family.phone());
        form.inputField("Пароль").type(family.password());
        form.inputField("Повторите пароль").type(family.password());
        ScenarioSession.set(USER, family);
        ScenarioSession.set(MAIL_BOX, mailBox);
    }
}
