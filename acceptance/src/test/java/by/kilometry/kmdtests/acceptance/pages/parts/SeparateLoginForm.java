package by.kilometry.kmdtests.acceptance.pages.parts;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

import static by.kilometry.kmdtests.acceptance.framework.util.StreamApi.toUnmodifiableList;
import static java.lang.String.format;
import static java.util.Collections.singletonList;

public class SeparateLoginForm extends Part {
    public SeparateLoginForm(WebElementFacade root) {
        super(root);
    }

    public WebElementFacade inputField(String name) {
        return inputFields(singletonList(name)).
                iterator().
                next();
    }

    public List<WebElementFacade> inputFields(List<String> names) {
        return names.stream().
                <WebElementFacade>map(name -> {
                    String inputWithPlaceholder = format("input[placeholder='%s']", name);
                    return root.findBy(By.cssSelector(inputWithPlaceholder));
                }).
                collect(toUnmodifiableList());
    }

    public WebElementFacade button(String name) {
        return buttons(singletonList(name)).
                iterator().
                next();
    }

    public List<WebElementFacade> buttons(List<String> names) {
        return names.stream().
                map(this::childButtonWithText).
                collect(toUnmodifiableList());
    }

    public List<WebElementFacade> registrationLinks(List<String> texts) {
        return texts.stream().
                map(this::childButtonWithText).
                collect(toUnmodifiableList());
    }
}