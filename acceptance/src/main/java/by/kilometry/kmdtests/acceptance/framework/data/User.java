package by.kilometry.kmdtests.acceptance.framework.data;

import by.kilometry.kmdtests.acceptance.framework.util.PropertiesFileLoader;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class User {
    private String firstname;
    private String lastname;
    private String middlename;
    private String email;
    private String phone;
    private String password;

    public User() {
    }

    public String firstname() {
        return firstname;
    }

    public String lastname() {
        return lastname;
    }

    public String middlename() {
        return middlename;
    }

    public String email() {
        return email;
    }

    public String phone() {
        return phone;
    }

    public String password() {
        return password;
    }

    public User firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public User lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public User middlename(String middlename) {
        this.middlename = middlename;
        return this;
    }

    public User email(String email) {
        this.email = email;
        return this;
    }

    public User phone(String phone) {
        this.phone = phone;
        return this;
    }

    public User password(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public static User fromPropertiesFile(final String fileName) {
        PropertiesConfiguration properties = PropertiesFileLoader.load(fileName);
        return new User().
                firstname(properties.getString("firstname")).
                lastname(properties.getString("lastname")).
                middlename(properties.getString("middlename")).
                email(properties.getString("email")).
                phone(properties.getString("phone")).
                password(properties.getString("password"));
    }
}
