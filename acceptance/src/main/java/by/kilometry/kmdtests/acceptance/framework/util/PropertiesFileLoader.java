package by.kilometry.kmdtests.acceptance.framework.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;

import static by.kilometry.kmdtests.acceptance.framework.Settings.DEFAULT_ENCODING;
import static java.lang.String.format;
import static java.lang.invoke.MethodHandles.lookup;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Convenient wrapper for {@link org.apache.commons.configuration.PropertiesConfiguration}: takes care about source
 * encoding and error handling.
 */
public class PropertiesFileLoader {
    private static final Logger LOG = getLogger(lookup().lookupClass());

    private PropertiesFileLoader() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    public static PropertiesConfiguration load(final String fileName) {
        try {
            LOG.debug("Load file [{}] using encoding [{}].", fileName, DEFAULT_ENCODING);
            final PropertiesConfiguration properties = new PropertiesConfiguration();
            properties.setEncoding(DEFAULT_ENCODING);
            properties.load(fileName);
            return properties;
        } catch (ConfigurationException onLoadingException) {
            String message = format("Failed to load properties from [%s] due to [%s].",
                    fileName,
                    onLoadingException);
            throw new IllegalStateException(message, onLoadingException);
        }
    }

    public static String loadHostProperty(String fileName) {
        String host = load(fileName).
                getString("host");
        LOG.debug("Host property is [{}].", host);
        return host;
    }
}
