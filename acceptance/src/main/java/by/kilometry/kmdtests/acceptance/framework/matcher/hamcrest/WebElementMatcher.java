package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest;

import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.TypeSafeMatcher;

public abstract class WebElementMatcher extends TypeSafeMatcher<WebElementFacade> {
    protected WebElementFacade element;
}
