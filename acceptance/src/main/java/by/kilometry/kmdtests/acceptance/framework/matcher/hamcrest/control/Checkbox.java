package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.control;

import by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.WebElementMatcher;
import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

public class Checkbox extends WebElementMatcher {
    private Checkbox() {
    }

    @Override
    protected boolean matchesSafely(WebElementFacade element) {
        this.element = element;
        Explicit.highlight(element);
        if (!element.isSelected()) {
            return false;
        }
        Explicit.putAway(element);
        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a selected checkbox [").
                appendText(element.toString()).
                appendText("]");
    }

    @Override
    protected void describeMismatchSafely(WebElementFacade ignored, Description mismatchDescription) {
        mismatchDescription.appendText("not selected checkbox [").
                appendText(element.toString()).
                appendText("]");
    }

    @Factory
    public static Matcher<WebElementFacade> selected() {
        return new Checkbox();
    }
}
