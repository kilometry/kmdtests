package by.kilometry.kmdtests.acceptance.framework.matcher.highlighting;

import net.serenitybdd.core.pages.WebElementFacade;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.openqa.selenium.StaleElementReferenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static by.kilometry.kmdtests.acceptance.framework.util.JoinPointExtractors.webElementFacade;
import static java.lang.invoke.MethodHandles.lookup;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Draws colored box around web-elements automatically.
 * <p>
 * There is no need to call any method of this class explicitly: AspectJ advices intercept certain actions that's
 * performed with web-elements.
 * <p>
 * Those advices react only to so-called "void actions". For example, if you click on a button a colored box will be
 * drawn around it. Or if you check that text field is visible then the field will be highlighted.
 * <p>
 * But this class ignores actions that obtain data from an element. For example, if you get value of text field  (via
 * {@link net.serenitybdd.core.pages.WebElementFacade#getText()}) you have to highlight the field explicitly with
 * aid of {@link by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit}.
 */
@Aspect
public class Implicit {
    /**
     * This is a list of interactions with web-elements that should be intercepted. As you can see, we just combine
     * several pointcuts.
     * <p>
     * Yes, there are a plenty of duplicated stuff in the constant. But we cannot assemble it dynamically: values of
     * Java-annotations must be resolved during compile-time.
     * <p>
     * Please keep style of the expression consistent, e.g. put each method signature, logical OR sign at separate lines
     * of the source code. And don't forget about spaces between them.
     */
    private static final String INTERACTION_WITH_ELEMENT = "" +
            "execution(public void net.serenitybdd.core.pages.WebElementFacade.click())" +
            " || " +
            "execution(public void net.serenitybdd.core.pages.WebElementState.shouldBeVisible())" +
            " || " +
            "execution(public * net.serenitybdd.core.pages.WebElementFacade.type(String))";

    @Pointcut(INTERACTION_WITH_ELEMENT)
    public void interactionWithElement() {
    }

    @Before("interactionWithElement()")
    public void beforeInteractionWithElement(JoinPoint pointcut) {
        Explicit.highlight(webElementFacade(pointcut));
    }

    @After("interactionWithElement()")
    public void afterInteractionWithElement(JoinPoint pointcut) {
        WebElementFacade element = webElementFacade(pointcut);
        try {
            Explicit.putAway(element);
        } catch (StaleElementReferenceException exceptionToLog) {
            // Once the element has been clicked it may be removed from the DOM
            // (and it happens in case of major number of dynamic pages).
            // There is no other way to check that element is stale rather than
            // catch the exception. For more details, please look at
            // "Check if a WebElement is stale without handling an exception"
            // (https://stackoverflow.com/a/25421826/4672928) topic.
            //
            // Obviously, we don't need to re-throw the exception. But appropriate
            // record in log can be helpful.
            LOG.debug("Failed to put away highlighting of element [{}]; " +
                            "attendant exception has been suppressed.",
                    element,
                    exceptionToLog);
        }
    }

    private static final Logger LOG = getLogger(lookup().lookupClass());
}