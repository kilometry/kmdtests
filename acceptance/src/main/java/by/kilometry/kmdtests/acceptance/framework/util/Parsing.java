package by.kilometry.kmdtests.acceptance.framework.util;

import java.util.Objects;
import java.util.regex.Matcher;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.regex.Pattern.compile;

public class Parsing {
    private static String KILOMETRES_REGEX = "(\\d+) км\\.";
    private static String BYN_REGEX = "(\\d+) BYN";

    private Parsing() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    public static int kilometres(String balance) {
        requireNonNull(balance);
        requireNonEmpty(balance);
        return parseIntUsingRegex(balance, KILOMETRES_REGEX);
    }

    public static int byn(String amount) {
        requireNonNull(amount);
        requireNonEmpty(amount);
        return parseIntUsingRegex(amount, BYN_REGEX);
    }

    private static void requireNonEmpty(String inputString) {
        if (inputString.isEmpty())
            throw new IllegalArgumentException("Input string should not be empty.");
    }

    private static void requireNonNull(String inputString) {
        Objects.requireNonNull(inputString, "Input string should not be null.");
    }

    private static int parseIntUsingRegex(String input, String regex) {
        Matcher matcher = compile(regex).
                matcher(input);
        if (!matcher.matches()) {
            String message = format("Failed to parse string [%s] " +
                            "using regular expression [%s].",
                    input,
                    regex);
            throw new IllegalStateException(message);
        }
        return parseInt(matcher.group(1));
    }
}
