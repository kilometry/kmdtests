package by.kilometry.kmdtests.acceptance.framework.session;

import net.serenitybdd.core.Serenity;

public class ScenarioSession {
    private ScenarioSession() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    public static <V> void set(SessionKey key, V value) {
        Serenity.setSessionVariable(key).to(value);
    }

    public static <V> V get(SessionKey key, Class<V> clazz) {
        Object value = Serenity.sessionVariableCalled(key);
        return clazz.cast(value);
    }
}
