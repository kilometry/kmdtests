package by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail;

import by.kilometry.kmdtests.acceptance.framework.email.MailBox;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import static by.kilometry.kmdtests.acceptance.framework.Settings.DEFAULT_ENCODING;
import static by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail.ApiKey.getFromEnvironment;
import static by.kilometry.kmdtests.acceptance.framework.util.StreamApi.lineByLine;
import static java.lang.String.format;
import static java.lang.invoke.MethodHandles.lookup;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
import static org.apache.http.util.EntityUtils.consume;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * <a href="https://temp-mail.org/">TempMail</a> is a service that offers free temporary e-mail boxes. You can work
 * with a box as with ordinary web-mail in your web-browser. Such functionality is pretty good for end-users but not for
 * automated tests.
 * <p>
 * Interaction with a box via API (for example, getting e-mails) is better approach in case of automated tests. In
 * addition to human-friendly interface, the service offers REST API but not for free. For instance,
 * <a href="https://market.mashape.com/Privatix/temp-mail/pricing">BASIC plan</a> allows only 100 API calls per day.
 * So some methods of this class should be used judiciously in order to do not exceed the quota. And, obviously, you
 * have to specify your API key in advance.
 * <p>
 * API key should be specified as <strong>environment variable</strong> (but not Java's system property due to security
 * reason: Java's system properties are visible in command line that can be accidentally stay in the log of build
 * server, etc). For the name of the variable please look at {@link ApiKey#API_KEY_ENVIRONMENT_VARIABLE}.
 */
public class TempMail implements MailBox {
    private static final Logger LOG = getLogger(lookup().lookupClass());

    private static final String TEMP_MAIL_WEBSITE = "https://temp-mail.org/en/";
    private static final String GET_EMAILS_URL_TEMPLATE = "https://privatix-temp-mail-v1.p.mashape.com/request/mail/id/%s/";

    private final String address;

    private TempMail(String address) {
        this.address = address;
    }

    /**
     * Creates new temporary e-mail box.
     * <p>
     * In contrast to the major part of methods in this class, this one doesn't make any API calls (and, as consequence,
     * doesn't require API key set). It can be invoked as many times as you want without running out of API calls
     * quota.
     * <p>
     * Such approach was chosen not because of "API call cheapness". <b>TempMail</b> service simply doesn't provide API
     * method for creating new box! So we have to mimic a web-browser: request a page, parse it and extract e-mail
     * address from HTML.
     * <p>
     * The downside of this method is low reliability. The method can fail if markup of the page will be altered. And
     * the failure will be not so obvious. We cannot predict all parsing issues but we try to provide as much
     * information as possible in exception message.
     *
     * @return an wrapper of newly created mail box. You can always obtain the exact address by appealing to {@link
     * MailBox#address()} method.
     */
    public static TempMail newBox() {
        HttpGet httpGet = new HttpGet(TEMP_MAIL_WEBSITE);
        try (CloseableHttpClient client = HttpClients.createDefault();
             CloseableHttpResponse response = client.execute(httpGet)) {
            ensureHttpOk(response);
            String address = emailAddressFromResponse(response);
            return new TempMail(address);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to create new e-mail box.", e);
        }
    }

    @Override
    public String address() {
        return address;
    }

    @Override
    public boolean isEmpty() {
        return allEmails().length() == 0;
    }

    @Override
    public String getAccountActivationLink() {
        String emailText = theMostRecentEmail();
        return Parser.accountActivationLinkFromEmail(emailText);
    }

    /**
     * Returns textual representation of the mail box.
     *
     * @return text that includes standard Java's description of the object (e.g. class name and address of the object
     * in memory) as well as e-mail address which is associated with the object. For example,
     * <i>
     * Temp Mail box [by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail.TempMail@2af9833a] with address
     * [satafucuz@sfamo.com]
     * </i>.
     *
     * @see TempMail#address
     */
    @Override
    public String toString() {
        return format(
                "Temp Mail box [%s] with address [%s].",
                super.toString(),
                this.address()
        );
    }

    private String theMostRecentEmail() {
        JSONObject emailJson = allEmails().
                getJSONObject(0);
        return Parser.textFromEmail(emailJson);
    }

    private JSONArray allEmails() {
        String url = format(GET_EMAILS_URL_TEMPLATE, md5Hex(address));
        String apiKey = getFromEnvironment();
        GetRequest request = Unirest.get(url).
                header("X-Mashape-Key", apiKey);
        try {
            HttpResponse<JsonNode> response = request.asJson();
            ensureHttpOk(response);
            JSONArray emails = emailsFromResponse(response);
            LOG.debug("Mail box [{}] contains [{}] email(s).", this.address, emails.length());
            return emails;
        } catch (UnirestException e) {
            String message = format("Failed to get " +
                            "the contents of mail box [%s] " +
                            "via HTTP request [%s] " +
                            "using URL [%s].",
                    this.address,
                    request.getHttpMethod(),
                    url);
            throw new IllegalStateException(message, e);
        }
    }

    private static JSONArray emailsFromResponse(final HttpResponse<JsonNode> response) {
        JSONArray potentialEmails = response.getBody().getArray();
        if (potentialEmails.length() == 1) {
            JSONObject message = potentialEmails.getJSONObject(0);
            if (message.has("error")) {
                return new JSONArray();
            }
        }
        return potentialEmails;
    }

    private static String emailAddressFromResponse(CloseableHttpResponse response) {
        String htmlPage = responseText(response);
        return Parser.emailAddressFromHtmlPage(htmlPage);
    }

    private static String responseText(final CloseableHttpResponse response) {
        HttpEntity entity = response.getEntity();
        try (InputStream content = entity.getContent()) {
            String text = IOUtils.toString(content, DEFAULT_ENCODING);
            consume(entity);
            return text;
        } catch (IOException e) {
            String message = format("Failed to convert HTTP response [%s] to text using encoding [%s].",
                    response,
                    DEFAULT_ENCODING);
            throw new IllegalStateException(message, e);
        }
    }

    private static void ensureHttpOk(CloseableHttpResponse response) {
        int status = response.getStatusLine().getStatusCode();
        if (status == HttpStatus.SC_OK) return;
        throw HttpExceptions.notOk(response);
    }

    private static void ensureHttpOk(HttpResponse response) {
        int status = response.getStatus();
        if (status == HttpStatus.SC_OK) return;
        throw HttpExceptions.notOk(response);
    }

    private static class HttpExceptions {
        static IllegalStateException notOk(HttpResponse response) {
            String message = format("Expected HTTP 200 but was HTTP [%d] in response [%s].",
                    response.getStatus(),
                    response.getBody());
            return new IllegalStateException(message);
        }

        static IllegalStateException notOk(CloseableHttpResponse response) {
            String message = format("Expected HTTP 200 but was HTTP [%d] in response [%s].",
                    response.getStatusLine().getStatusCode(),
                    response.getEntity());
            return new IllegalStateException(message);
        }
    }
}

/**
 * Helps to parse HTML source.
 * <p>
 * It's possible to fetch HTML-pages line by line from remote server. Such approach is really efficient in case of huge
 * amount of text. But in case of TempMail service the pages (as well as content of e-mails which is also written in
 * HTML) are not so big. There is no need to download them line by line. From the other side, it's much better to get
 * "full copy" of text before parsing: we can log the text in case of failure. That will be very helpful during
 * investigation (especially because of general fragility of HTML parsing).
 */
class Parser {
    private static final Logger LOG = getLogger(lookup().lookupClass());

    private static class Regex {
        static final String INPUT_TAG = ".*<input.*id=\"mail\".*>.*";
        static final String VALUE_ATTRIBUTE = ".*value=\"(\\S+?@\\S+?\\.\\S+?)\".*";
        static final String ACCOUNT_ACTIVATION_LINK = ".*href=\"(.*)\".*";
    }

    private static final String MAIL_TEXT_JSON_KEY = "mail_text_only";

    static String emailAddressFromHtmlPage(final String pageSource) {
        if (pageSource.isEmpty()) {
            throw new IllegalArgumentException("Source of HTML pages should not be empty.");
        }
        // We parse HTML page in two steps:
        //
        // 1. Filter strings that contains INPUT tag with certain id
        // 2. Extract e-mail address from the string found on step 1.
        //
        // Yes, it's possible to write single regular expression that does the same
        // thing. But the resultant expression will be too complex. This is why we
        // break it into two parts.
        return lineByLine(pageSource).
                filter(line -> compile(Regex.INPUT_TAG).matcher(line).matches()).
                map(line -> compile(Regex.VALUE_ATTRIBUTE).matcher(line)).
                filter(Matcher::matches).
                map(matcher -> matcher.group(1)).
                peek(extractedAddress -> LOG.debug("Extracted e-mail address is [{}].", extractedAddress)).
                findFirst().
                orElseThrow(() -> {
                    String message = format("Failed to extract e-mail address from HTML page [%s] " +
                                    "using a chain of regular expressions [%s] and [%s].",
                            pageSource,
                            Regex.INPUT_TAG,
                            Regex.VALUE_ATTRIBUTE);
                    return new IllegalStateException(message);
                });
    }

    static String accountActivationLinkFromEmail(final String emailText) {
        if (emailText.isEmpty()) {
            throw new IllegalArgumentException("Text of e-mail should not be empty.");
        }
        return lineByLine(emailText).
                map(line -> compile(Regex.ACCOUNT_ACTIVATION_LINK).matcher(line)).
                filter(Matcher::matches).
                map(matcher -> matcher.group(1)).
                peek(link -> LOG.debug("Extracted link for account activation is [{}].", link)).
                findFirst().
                orElseThrow(() -> {
                    String message = format("Cannot extract link for account activation " +
                                    "from e-mail text [%s] using regular expression [%s].",
                            emailText, Regex.ACCOUNT_ACTIVATION_LINK);
                    return new IllegalStateException(message);
                });
    }

    static String textFromEmail(final JSONObject emailJson) {
        if (!emailJson.has(MAIL_TEXT_JSON_KEY)) {
            String message = format("E-mail JSON [%s] doesn't contain [%s] key.",
                    emailJson.toString(2),
                    MAIL_TEXT_JSON_KEY);
            throw new IllegalArgumentException(message);
        }
        return emailJson.getString(MAIL_TEXT_JSON_KEY);
    }
}