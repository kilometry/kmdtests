/**
 * Welcome to Acceptance Tests for the website!
 * <p>
 * The major part of the tests is done with aid of Serenity BDD + JBehave. We do the utmost of us in order to make the
 * tests perceptible and robust. This package provides additional features like working with e-mails,
 * <i>.properties</i>-files, Selenium web-driver and a few more.
 * <p>
 * As the amount of auxiliary stuff grows and the utilities become sophisticated we need to test them. It sounds
 * bombastic but we have to write tests for the utilities which are used in Acceptance Tests! This is why we keep a
 * package for Unit Tests with the same name under Maven's <i>test</i> folder.
 * <p>
 * As for documentation... Well, this  document is pure Javadoc. We can cover mostly Java and related things here. So
 * other valuable knowledge about the tests is located in README-files, in comments inside the shell-scripts and
 * everywhere in source code repository.
 */
package by.kilometry.kmdtests.acceptance.framework;