package by.kilometry.kmdtests.acceptance.framework.util;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class StreamApi {
    private StreamApi() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    /**
     * Just saves time on typing parameters in {@link Stream#collect(java.util.stream.Collector)} methods.
     */
    public static <T> Collector<T, ?, List<T>> toUnmodifiableList() {
        return collectingAndThen(toList(), Collections::unmodifiableList);
    }

    /**
     * Splits the text by line endings platform-independently. E.g. handles variations of CR-LF, LF and CR gracefully.
     * <p>
     * For more details please read the answer for
     * <a href="https://stackoverflow.com/a/31060125/4672928">Split Java String by New Line</a>
     * question.
     *
     * @param text text to be split
     *
     * @return stream of individual lines. Trailing empty lines will be included.
     */
    public static Stream<String> lineByLine(String text) {
        return Stream.of(text.split("\\R"));
    }
}
