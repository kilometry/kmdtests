package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text;

import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Iterator;
import java.util.List;

/**
 * Checks that every web-element in the list has corresponding text.
 * <p>
 * Text values are compared in the same order as they are kept in the lists. If number of web-elements is greater than
 * number of expected text values (or vice versa) no error will be thrown, the rest will be just skipped.
 * <p>
 * If comparison for one element fails the rest will be skipped also. There is no need to verify other elements because
 * that will bloat assertion message. As opposite, simple assertion message is more easy to confront with screenshot.
 * <p>
 * The verification is exact, e.g. glob patterns and/or regular expressions are not supported yet.
 */
public class Texts extends TypeSafeMatcher<List<WebElementFacade>> {
    private final List<String> expectedTexts;

    private String currentlyExpectedText;
    private WebElementFacade currentElement;
    private String currentlyActualText;

    private Texts(final List<String> expectedTexts) {
        this.expectedTexts = expectedTexts;
    }

    @Override
    protected boolean matchesSafely(final List<WebElementFacade> elements) {
        final Iterator<WebElementFacade> iteratorOverElements = elements.iterator();
        final Iterator<String> iteratorOverTexts = expectedTexts.iterator();
        while (iteratorOverElements.hasNext() &&
                iteratorOverTexts.hasNext()) {
            currentElement = iteratorOverElements.next();
            Explicit.highlight(currentElement);
            currentlyActualText = currentElement.getText();
            currentlyExpectedText = iteratorOverTexts.next();
            if (!currentlyActualText.equals(currentlyExpectedText)) {
                return false;
            }
            Explicit.putAway(currentElement);
        }
        return true;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("a web-element [").
                appendText(currentElement.toString()).
                appendText("] ").
                appendText("with text [").
                appendText(currentlyExpectedText).
                appendText("]");
    }

    @Override
    protected void describeMismatchSafely(List<WebElementFacade> ignored, Description mismatchDescription) {
        mismatchDescription.appendText("text was [").
                appendText(currentlyActualText).
                appendText("]; verification of rest of the elements has been skipped.");
    }

    @Factory
    public static Matcher<List<WebElementFacade>> equalTo(List<String> expectedTexts) {
        return new Texts(expectedTexts);
    }
}
