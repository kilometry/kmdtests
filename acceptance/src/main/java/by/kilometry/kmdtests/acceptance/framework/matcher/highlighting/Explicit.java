package by.kilometry.kmdtests.acceptance.framework.matcher.highlighting;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.Pages;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.Optional;

import static java.lang.String.format;

/**
 * Utility for drawing colored box around a web-element.
 * <p>
 * If you would like to make emphasis on element before interaction with it (for example, you would like to accentuate
 * typing into text field) then call {@link Explicit#highlight(net.serenitybdd.core.pages.WebElementFacade)} method.
 * <p>
 * After interaction with the element just call {@link Explicit#putAway(net.serenitybdd.core.pages.WebElementFacade)}.
 * Generally, it's a good practice to clearly indicate that you've done all the job.
 * <p>
 * Currently "color before interaction" is Red and "color after interaction" is Green. This can be changed in the
 * future. This is why names of the methods are not tied to concrete colors.
 * <p>
 * As for implementation of this method... Well, it's weird.
 * <p>
 * We need an instance of web-driver in order to execute JavaScript (that draws a box). But we cannot legally obtain an
 * instance of the driver outside of {@code PageObject}. Actually, it's possible to get web-driver via {@link
 * Pages#getDriver()} but it doesn't work: the driver returned is always equal to {@code null}. Such behavior isn't
 * surprising. As said in the documentation of {@link Pages} class:
 * <blockquote>
 * The Pages object keeps track of what web pages a test visits, and helps with mapping pages to Page Objects. A Pages
 * object is associated with a WebDriver driver instance, so you need a Pages object for any given WebDriver driver.
 * </blockquote>
 * <p>
 * It's possible to intercept setting {@code driver} field of {@link PageObject} with aid of AOP. And save the most
 * recent web-driver instance in static field. Such approach was implemented (please search for
 * <i>TheMostRecentWebDriver</i> class name in SCM history)! But plumbing private field is fragile in general (because
 * name of private field can be changed "without prior notice" by the developers of Serenity BDD) and because of
 * Serenity in particular. As already said above, page object is associated with a driver instance. Probably,
 * interacting with a page using inappropriate instance of web-driver will lead to mysterious failures.
 * <p>
 * We use another but simple hack: call {@link WebElementFacade#waitForCondition()} method on a web-element. We don't
 * wait for any condition actually. We highlight (or put away) an element and return {@code true} immediately. Tt's
 * really robust approach because that method provides an instance of web-driver that corresponds to given element (and,
 * as consequence, to right instance of {@link PageObject}).
 */
public class Explicit {
    public static void highlight(WebElementFacade element) {
        element.waitForCondition().
                until(driver -> {
                    javascriptExecutorOf(driver).
                            executeScript(HIGHLIGHT, element);
                    return true;
                });
    }

    public static void putAway(WebElementFacade element) {
        element.waitForCondition().
                until(driver -> {
                    javascriptExecutorOf(driver).
                            executeScript(PUT_AWAY, element);
                    return true;
                });
    }

    private static JavascriptExecutor javascriptExecutorOf(WebDriver webDriver) {
        JavascriptExecutor executor = JavascriptExecutor.class.cast(webDriver);
        return Optional.ofNullable(executor).
                orElseThrow(() -> new IllegalStateException("Failed " +
                        "to extract JavascriptExecutor instance " +
                        "from web-driver because the driver is null."));
    }

    private static final String JAVA_SCRIPT_TEMPLATE = "arguments[0].style.border='3px solid %s'";
    private static final String HIGHLIGHT = format(JAVA_SCRIPT_TEMPLATE, "red");
    private static final String PUT_AWAY = format(JAVA_SCRIPT_TEMPLATE, "green");
}
