package by.kilometry.kmdtests.acceptance.framework.util;

import by.kilometry.kmdtests.acceptance.framework.email.MailBox;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

public class MailProvider {
    private MailProvider() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    /**
     * Checks that a mail box became non-empty.
     * <p>
     * This method checks the box. If the box is not empty it returns immediately. If the box is still empty than the
     * method waits for a certain amount of time and repeats the process again and again.
     * <p>
     * Name of the method looks a little bit cryptic while being read in isolation. But it looks very nicely when used
     * in combination with fluent wait-expressions provided by third-party libraries, for example,
     * <a href="https://github.com/awaitility/awaitility">Awaitility</a>.
     *
     * @param mailBox              a box to check
     * @param maxAttempts          maximum number of attempts, e.g. maximum number of allowed invocations of {@link
     *                             MailBox#isEmpty()} method. This parameter was introduced not only for avoiding
     *                             infinite loop but for limiting a number of API calls in order to do not exceed API
     *                             calls quota. This parameter should be greater than zero.
     * @param pauseBetweenAttempts a delay between interaction with the mail box. There is no need to check the box
     *                             right after previous verification: new message(s) can arrive sooner or later. Also,
     *                             this parameter helps to do not overwork remote server (a lot of subsequent requests
     *                             cah be considered by the server as DoS; such situation is hardly probable in case of
     *                             "free" or "basic" plans but anyway). The duration should not be less than half of
     *                             second.
     *
     * @return {@code true} if the box is not empty, {@code false} otherwise
     *
     * @see MailBox#isEmpty()
     */
    public static boolean anEmailReceived(@NotNull final MailBox mailBox,
                                          final int maxAttempts,
                                          @NotNull final Duration pauseBetweenAttempts) {
        requireNonNull(mailBox, "Mail box wrapper should not be null.");
        requireMaxAttemptsGreaterThanZero(maxAttempts);
        requireNonNull(pauseBetweenAttempts, "Pause between attempts should not be null.");
        requirePauseNotLessThanHalfOfSecond(pauseBetweenAttempts);

        for (int attempt = 1; attempt <= maxAttempts; attempt++) {
            if (!mailBox.isEmpty()) return true;
            else pause(mailBox, attempt, pauseBetweenAttempts);
        }
        return false;
    }

    private static void requireMaxAttemptsGreaterThanZero(int maxAttempts) {
        if (maxAttempts <= 0) {
            String message = format("Maximum number of attempts should be " +
                            "greater than zero but [%d] was specified.",
                    maxAttempts);
            throw new IllegalArgumentException(message);
        }
    }

    private static void requirePauseNotLessThanHalfOfSecond(Duration pauseBetweenAttempts) {
        final Duration halfOfSecond = Duration.ofSeconds(1).dividedBy(2);
        if (pauseBetweenAttempts.compareTo(halfOfSecond) < 0) {
            String message = format("Pause between attempts should be " +
                            "at least half of second (e.g. 500 milliseconds) " +
                            "but [%d] milliseconds was specified.",
                    pauseBetweenAttempts.toMillis()
            );
            throw new IllegalArgumentException(message);
        }
    }

    private static void pause(MailBox mailBox, int currentAttempt, Duration pauseBetweenAttempts) {
        try {
            TimeUnit.NANOSECONDS.sleep(pauseBetweenAttempts.toNanos());
        } catch (InterruptedException onInterrupted) {
            String message = format("Interrupted while checking for emptiness of mail box [%s] " +
                            "at attempt [%d] " +
                            "with pause [%s] milliseconds.",
                    mailBox,
                    currentAttempt,
                    pauseBetweenAttempts.toMillis());
            throw new IllegalStateException(message, onInterrupted);
        }
    }
}
