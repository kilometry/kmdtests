package by.kilometry.kmdtests.acceptance.framework.session;

public enum  SessionKey {
    USER,
    MAIL_BOX,
    ACCOUNT_ACTIVATION_LINK,
    PAYMENT_AMOUNT,
    BALANCE
}
