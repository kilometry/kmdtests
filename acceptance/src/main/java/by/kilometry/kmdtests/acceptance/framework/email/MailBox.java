package by.kilometry.kmdtests.acceptance.framework.email;

/**
 * Represents temporary mail box.
 * <p>
 * This interface was designed from the perspective of user registration on the site. So the interface specifies very
 * limited set of operations.
 * <p>
 * User registration is quite quick process. It typically takes not more than 10 minutes (or even less in case of
 * automated tests). So typical "Ten Minute E-mail" service will fair choice for implementation of the interface.
 */
public interface MailBox {
    /**
     * Returns address of the box.
     * <p>
     * Address of the box cannot be changed on remote server. E.g. new address means completely new mail box. So an
     * implementation of this method should NOT interact with the server but should return cached value instead.
     *
     * @return e-mail address in ordinary textual form, for example, <i>wedow@topikt.com</i>
     */
    String address();

    /**
     * Checks that the box is empty.
     * <p>
     * An implementation of this method should give "fresh" state of the box, e.g. check that the box is really empty on
     * remote server but not return cached value.
     * <p>
     * As long as this method makes API call(s) please be aware of running out of the quota.
     *
     * @return {@code true} if the box is empty, {@code false} otherwise
     */
    boolean isEmpty();

    /**
     * Returns link for account activation.
     * <p>
     * After user submitted registration form he/she will receive an e-mail that contains a link for account activation.
     * The e-mail can include some explanatory text, for example:
     *
     * <pre>
     *     Спасибо за регистрацию на сайте Километры Добра!
     *
     *     Чтобы завершить регистрацию, Вам нужно подтвердить свой адрес электронной почты. Для этого, перейдите
     *     пожалуйста по этой link_for_account_activation_is_here.
     *
     *
     *     С уважением, команда Километров Добра.
     * </pre>
     * <p>
     * An implementation of this method should fetch the most recent e-mail from remote server and try to extract link
     * for account activation from the message.
     * <p>
     * As long as this method makes API call(s) please be aware of running out of the quota.
     *
     * @return link for account activation in textual form without any additional elements, like {@code href}, etc. E.g.
     * returned link is ready to be copied-and-pasted to address bar of a web-browser. For example,
     * <i>https://kmd-test-auth.azurewebsites.net/api/identity/confirmEmail?userId=de63d8b1-6805-47fb-a30f-...email-confirmed</i>
     */
    String getAccountActivationLink();
}
