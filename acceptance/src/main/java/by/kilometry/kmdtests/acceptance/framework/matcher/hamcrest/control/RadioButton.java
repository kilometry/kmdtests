package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.control;

import by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.WebElementMatcher;
import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

public class RadioButton extends WebElementMatcher {
    private static final boolean EXPECTED_VALUE_OF_CHECKED_ATTRIBUTE = true;

    private String rawActualValue;

    private RadioButton() {
    }

    @Override
    protected boolean matchesSafely(WebElementFacade element) {
        this.element = element;
        Explicit.highlight(element);
        rawActualValue = element.getAttribute("checked");
        boolean neatActualValue = Boolean.parseBoolean(rawActualValue);
        if (neatActualValue != EXPECTED_VALUE_OF_CHECKED_ATTRIBUTE) {
            return false;
        }
        Explicit.putAway(element);
        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a radio button [").
                appendText(element.toString()).
                appendText("] with 'checked' attribute equal to True");
    }

    @Override
    protected void describeMismatchSafely(WebElementFacade ignored, Description mismatchDescription) {
        mismatchDescription.appendText("'checked' attribute was [").
                appendText(rawActualValue).
                appendText("]");
    }

    @Factory
    public static Matcher<WebElementFacade> checked() {
        return new RadioButton();
    }
}
