package by.kilometry.kmdtests.acceptance.framework.util;

import com.jayway.awaitility.core.ConditionTimeoutException;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;
import net.thucydides.core.webdriver.exceptions.ElementShouldBeVisibleException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.slf4j.Logger;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static by.kilometry.kmdtests.acceptance.framework.Settings.SerenityProperties.webdriver_timeouts_implicitlywait;
import static com.jayway.awaitility.Awaitility.await;
import static com.jayway.awaitility.Duration.TWO_HUNDRED_MILLISECONDS;
import static java.lang.String.format;
import static java.lang.invoke.MethodHandles.lookup;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Name of this utility class is quite strange. And it's clashing with Selenium's {@link
 * org.openqa.selenium.WebElement}! But there is no need to worry about this. We don't call Selenium directly, but via
 * Serenity BDD framework. The framework provides highly useful analog: {@link net.serenitybdd.core.pages.WebElementFacade}.
 * Even more, using the same name protects us from involving Selenium explicitly (in case of clashing full-qualified
 * class names will appear in the source code, such long identifiers are very eye-noticeable).
 */
public class WebElement {
    private static final Logger LOG = getLogger(lookup().lookupClass());

    private WebElement() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    /**
     * Serenity BDD provides two methods that can be used in order to determine whether a web-element is visible: {@link
     * WebElementState#shouldBeCurrentlyVisible()} and {@link WebElementState#shouldBeVisible()}. Javadoc for those
     * method is the same:
     * <blockquote>
     * Checks whether a web element is visible.<br> Throws an AssertionError if the element is not rendered.
     * </blockquote>
     * <p>
     * And those methods actually do the same: throw an assertion if an element is not visible. But the methods doesn't
     * wait for element's visibility at all, e.g. standard Serenity's properties
     * <strong>webdriver.timeouts.implicitlywait</strong>, <strong>webdriver.wait.for.timeout</strong> and
     * <strong>serenity.timeout</strong> are not taken into account. So we need to execute
     * {@link WebElementFacade#waitUntilVisible()} method in advance.
     */
    public static void shouldBeInstantlyVisible(WebElementFacade element) {
        element.waitUntilVisible().
                shouldBeVisible();
    }

    /**
     * Ensures that an element has desired text instantly.
     * <p>
     * In major number of cases (e.g. when text of an element is changed without manipulation with DOM structure) using
     * of this method is not obligatory.
     * <p>
     * As is generally known, web-elements on a page can be changed dynamically by JavaScript. For example, an element
     * can be removed from DOM and then added to DOM again. That can lead to mysterious effects.
     * <p>
     * Lets consider a case when you have a page with text label. You get the label by CSS selector. If the label is
     * updated via JavaScript trivially (e.g. only text is changed) then you are OK. But the label can be updated in
     * other way that's identical to the latter case visually but different internally.
     * <p>
     * An element can be removed from DOM, then added to DOM at the same coordinates (e.g. the element can be accessed
     * by the same "path": XPath, CSS-selector or whatever) with new value of text. Looks not terrible but web-driver
     * can go into trouble. Possible issues are:
     * <ol>
     * <li>you apply to an element and get old version of it's text</li>
     * <li>you apply to an element and get stale element exception</li>
     * <li>you apply to an element and get time out error because element is not visible</li>
     * </ol>
     * <p>
     * Process of detaching an element from DOM and attaching it again is not atomic. E.g. it's not executed in one
     * step. Also, a web-browser can update elements on a page visually but Selenium Web Driver can still see the old
     * "snapshot" of DOM. This is the reason of the issues listed above.
     * <p>
     * In order to get rid of the issues we can pull an element (with some reasonable delay) and catch exceptions until
     * an element re-attached with desired text.
     * <p>
     * For the sake of consistency with other web-driver's operations  default timeout, e.g. value of Serenity BDD
     * property <strong>webdriver.timeouts.implicitlywait</strong> will be used as maximum duration of execution of this
     * method.
     *
     * @param element      supplier that returns new instance of element (in terms of Serenity BDD) on each {@code
     *                     get()}. Appealing to new instance each time is required in order to avoid false-positive
     *                     exceptions.
     * @param expectedText text to be appeared on the element. The comparison is exact.
     *
     * @throws ConditionTimeoutException if the timeout was passed but the element hasn't appeared in DOM with expected
     *                                   text
     */
    public static void waitUntilReAttachedWithText(@NotNull final Supplier<WebElementFacade> element,
                                                   @NotNull final String expectedText) {
        String alias = format("Wait for re-attachment of element [%s] with text [%s].",
                element.get(),
                expectedText);
        await(alias).
                pollInterval(TWO_HUNDRED_MILLISECONDS).
                atMost(webdriver_timeouts_implicitlywait().toMillis(), TimeUnit.MILLISECONDS).
                until(() -> reAttachedWithText(element, expectedText));
    }

    private static boolean reAttachedWithText(final Supplier<WebElementFacade> element,
                                              final String expectedText) {
        // We stupidly run infinite loop end break it when the element has got desired text.
        // This method should be used only with combination with fluent-waits from Awaitility
        // library. Otherwise the loop will never end.
        //
        // Awaitility library terminates the threads that correspond to condition in `until()'
        // method. But in Java a thread cannot be terminated without communication with it
        // (there is no `kill' command from Unix-world in Java). This is why we check
        // "interrupted" status inside the loop.
        //
        // Please find more details on this in the answer for:
        // "How to Stop long duration execution task ( e.g. Infinite Loop) execution inside a Executor Service"
        // (https://stackoverflow.com/a/10858554/4672928)
        while (true) {
            try {
                String actualText = element.get().
                        withTimeoutOf(100, TimeUnit.MILLISECONDS).
                        getText();
                if (Objects.equals(actualText, expectedText))
                    return true;
                if (Thread.interrupted()) {
                    LOG.debug("Thread [{}] has been terminated; exit from infinite `while' loop.",
                            Thread.currentThread());
                    return false;
                }
            } catch (StaleElementReferenceException |
                    TimeoutException |
                    ElementShouldBeVisibleException onDetached) {
                // Exception is suppressed, but there is no reason to loose the exception completely.
                // Anyway, debug log messages can be easily disabled via SLF4J configuration file.
                LOG.debug("Element [{}] has been detached from DOM; " +
                                "attendant exception has been suppressed.",
                        element.get(),
                        onDetached
                );
            }
        }
    }
}
