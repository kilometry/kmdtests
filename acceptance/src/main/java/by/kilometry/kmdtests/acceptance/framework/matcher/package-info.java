/**
 * A bunch of Hamcrest matchers which are specific for Acceptance Tests.
 * <p>
 * Typically when you compare actual result with expected result you compare the whole data. For example, if you check
 * lists you don't compare them item by item but assert the whole lists at once, like this:
 * <pre>
 *     assertThat(actualList, equalTo(expectedList));
 * </pre>
 * <p>
 * The approach described above is very good in general. But Acceptance Tests that involve UI automation with aid of
 * Selenium is quite specific area. The results of such tests cannot be represented as text, we need understandable test
 * reports instead. The best way to represent result of an assertion is to take screenshot of a web-page.
 * <p>
 * So far so good. But how we figure out what element is invalid on a page (as long as typical web-page contains dozens
 * of UI elements)?
 * <p>
 * Just look at plain screenshot:
 * <br>
 * <img src="doc-files/without_highlighting.png" alt="Separate login form" width="320px">
 * <p>
 * And look at screenshot with highlighting:
 * <br>
 * <img src="doc-files/with_highlighting.png" alt="Separate login form" width="320px">
 * <p>
 * The latter gives much more information about what elements on the page were tested. Thus, elements which already have
 * been verified are colored in <i>Green</i>. Elements which haven't been tested yet are colored in <i>Red</i>. Elements
 * which are not a subject for test are just non-highlighted.
 * <p>
 * The last but not least, elements should be highlighted one by one during comparison. This makes tests perceptible,
 * e.g. you can track verification process in live mode (like watching a video). And underlying source code is easy to
 * understand as long as the process is implemented in Hamcrest-style. For example, we can check that buttons contain
 * certain text like this:
 * <pre>
 *     List&lt;WebElementFacade&gt; buttonLabels = getButtonLabels();
 *     List&lt;String&gt; expectedLabels = asList("Yes", "No", "May be");
 *     assertThat(buttonLabels, equalTo(expectedLabels));
 * </pre>
 * <p>
 * For details of the implementation, please look at
 * {@link by.kilometry.kmdtests.acceptance.framework.matcher.highlighting} sub-package.
 */
package by.kilometry.kmdtests.acceptance.framework.matcher;