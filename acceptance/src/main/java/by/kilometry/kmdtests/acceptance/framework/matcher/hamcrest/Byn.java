package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest;

import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import by.kilometry.kmdtests.acceptance.framework.util.Parsing;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static java.lang.String.format;

public class Byn extends WebElementMatcher {
    private final int expectedAmount;
    private int actualAmount;

    private Byn(int expectedAmount) {
        this.expectedAmount = expectedAmount;
    }

    @Override
    protected boolean matchesSafely(final WebElementFacade element) {
        this.element = element;
        Explicit.highlight(element);
        actualAmount = Parsing.byn(element.getText());
        if (actualAmount != expectedAmount) {
            return false;
        }
        Explicit.putAway(element);
        return true;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("a web-element [").
                appendText(element.toString()).
                appendText("] ").
                appendText("with amount in BYN [").
                appendText(format("%s", expectedAmount)).
                appendText("]");
    }

    @Override
    protected void describeMismatchSafely(WebElementFacade ignored, Description mismatchDescription) {
        mismatchDescription.appendText("amount was [").
                appendText(format("%s", actualAmount)).
                appendText("]");
    }

    @Factory
    public static Matcher<WebElementFacade> equalTo(int expectedAmount) {
        return new Byn(expectedAmount);
    }
}
