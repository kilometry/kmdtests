package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.kilometres;

import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import static java.lang.String.format;

public class BalanceExact extends BalanceMatcher {
    private final int expectedBalance;

    private BalanceExact(int expectedBalance) {
        this.expectedBalance = expectedBalance;
    }

    @Override
    protected boolean matchesSafely(final WebElementFacade element) {
        this.element = element;
        Explicit.highlight(element);
        actualBalance = balanceFromWebElement(element);
        if (actualBalance != expectedBalance) {
            return false;
        }
        Explicit.putAway(element);
        return true;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("a web-element [").
                appendText(element.toString()).
                appendText("] ").
                appendText("with balance in kilometres [").
                appendText(format("%s", expectedBalance)).
                appendText("]");
    }

    @Override
    protected void describeMismatchSafely(WebElementFacade ignored, Description mismatchDescription) {
        mismatchDescription.appendText("balance was [").
                appendText(format("%s", actualBalance)).
                appendText("]");
    }

    @Factory
    public static Matcher<WebElementFacade> equalTo(int expectedBalance) {
        return new BalanceExact(expectedBalance);
    }
}
