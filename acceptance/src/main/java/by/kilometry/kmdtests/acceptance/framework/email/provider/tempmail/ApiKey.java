package by.kilometry.kmdtests.acceptance.framework.email.provider.tempmail;

import java.util.Optional;

import static java.lang.String.format;

class ApiKey {
    public static final String API_KEY_ENVIRONMENT_VARIABLE = "by_kilometry_kmdtests_mail_tempmail_api_key";

    static String getFromEnvironment() {
        return Optional.
                ofNullable(System.getenv(API_KEY_ENVIRONMENT_VARIABLE)).
                orElseThrow(() -> {
                    String message = format("Environment variable [%s] is not set.", API_KEY_ENVIRONMENT_VARIABLE);
                    return new IllegalStateException(message);
                });
    }
}
