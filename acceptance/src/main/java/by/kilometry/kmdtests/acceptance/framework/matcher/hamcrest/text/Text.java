package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.text;

import by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.WebElementMatcher;
import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

/**
 * Checks that a web-element has specified text value.
 * <p>
 * The verification is exact, e.g. glob patterns and/or regular expressions are not supported yet.
 */
public class Text extends WebElementMatcher {
    private final String expectedText;
    private String actualText;

    private Text(String expectedText) {
        this.expectedText = expectedText;
    }

    @Override
    protected boolean matchesSafely(final WebElementFacade element) {
        this.element = element;
        Explicit.highlight(element);
        actualText = element.getText();
        if (!actualText.equals(expectedText)) {
            return false;
        }
        Explicit.putAway(element);
        return true;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("a web-element [").
                appendText(element.toString()).
                appendText("] ").
                appendText("with text [").
                appendText(expectedText).
                appendText("]");
    }

    @Override
    protected void describeMismatchSafely(WebElementFacade ignored, Description mismatchDescription) {
        mismatchDescription.appendText("text was [").
                appendText(actualText).
                appendText("]");
    }

    @Factory
    public static Matcher<WebElementFacade> equalTo(String expectedText) {
        return new Text(expectedText);
    }
}
