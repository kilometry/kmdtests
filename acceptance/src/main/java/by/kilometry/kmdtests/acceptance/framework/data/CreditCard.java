package by.kilometry.kmdtests.acceptance.framework.data;

import by.kilometry.kmdtests.acceptance.framework.util.PropertiesFileLoader;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class CreditCard {
    private String number;
    private String validThruMonth;
    private String validThruYear;
    private String cardholderName;
    private String cvv;

    public CreditCard() {
    }

    public String number() {
        return number;
    }

    public CreditCard number(String number) {
        this.number = number;
        return this;
    }

    public String validThruMonth() {
        return validThruMonth;
    }

    public CreditCard validThruMonth(String validThruMonth) {
        this.validThruMonth = validThruMonth;
        return this;
    }

    public String validThruYear() {
        return validThruYear;
    }

    public CreditCard validThruYear(String validThruYear) {
        this.validThruYear = validThruYear;
        return this;
    }

    public String cardholderName() {
        return cardholderName;
    }

    public CreditCard cardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
        return this;
    }

    public String cvv() {
        return cvv;
    }

    public CreditCard cvv(String cvv) {
        this.cvv = cvv;
        return this;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public static CreditCard fromPropertiesFile(final String fileName) {
        PropertiesConfiguration properties = PropertiesFileLoader.load(fileName);
        return new CreditCard().
                number(properties.getString("number")).
                validThruMonth(properties.getString("valid.thru.month")).
                validThruYear(properties.getString("valid.thru.year")).
                cardholderName(properties.getString("cardholder.name")).
                cvv(properties.getString("cvv"));
    }
}


