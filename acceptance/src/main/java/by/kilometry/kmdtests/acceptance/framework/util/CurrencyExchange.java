package by.kilometry.kmdtests.acceptance.framework.util;

public class CurrencyExchange {
    private static final double BYN_TO_KILOMETER_RATE = 0.7D;

    public static int bynToKilometer(int paymentAmount) {
        // Probably, Banker's rounding
        // (http://www.xbeat.net/vbspeed/i_BankersRounding.htm)
        // will be used instead of pure mathematical method.
        double rawKilometers = Math.round(paymentAmount / BYN_TO_KILOMETER_RATE);
        return (int) rawKilometers;
    }
}
