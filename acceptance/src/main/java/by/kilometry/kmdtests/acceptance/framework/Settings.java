package by.kilometry.kmdtests.acceptance.framework;

import net.thucydides.core.ThucydidesSystemProperty;
import org.slf4j.Logger;

import java.time.Duration;

import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.lang.invoke.MethodHandles.lookup;
import static java.util.Optional.ofNullable;
import static net.thucydides.core.util.SystemEnvironmentVariables.createEnvironmentVariables;
import static org.slf4j.LoggerFactory.getLogger;

public class Settings {
    private Settings() {
        throw new UnsupportedOperationException("This is utility class. Refer to its constants only.");
    }

    /**
     * UTF-8 is used by Maven as encoding for the Java-code as well as for resources (e.g. other files which are
     * processed during build).
     * <p>
     * But sometimes we need to read and parse a file (for example, <i>.properties</i>-file) manually. It will be good
     * practice to use the same encoding everywhere.
     * <p>
     * However, when working with HTTP we need to take into account actual encoding specified in HTTP header or in HTML
     * <i>meta</i> tag. Hopefully, UTF-8 is ubiquitous. So we should refer to this constant as often as possible.
     */
    public static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * Provides values of Serenity BDD's properties programmatically.
     * <p>
     * Methods in this class aren't named according to Java-conventions because of simplicity. In
     * <i>.properties</i>-files for Serenity BDD each names is specified using dot-notation, like this:
     * <pre>
     * ...
     * webdriver.timeouts.implicitlywait=20000
     * webdriver.wait.for.timeout=20000
     * ...
     * </pre>
     * <p>
     * We mimic to those names (but use underscore instead of dot because the latter is not a valid character in
     * method's name).
     * <p>
     * This class performs parsing and conversion from string values into appropriate Java types. For example,
     * properties that specify timeouts will be returned as instances of {@see java.time.Duration}.
     */
    public static class SerenityProperties {
        private static final Logger LOG = getLogger(lookup().lookupClass());

        public static Duration webdriver_timeouts_implicitlywait() {
            String rawMillis = rawValueOf(ThucydidesSystemProperty.WEBDRIVER_TIMEOUTS_IMPLICITLYWAIT);
            return Duration.ofMillis(parseLong(rawMillis));
        }

        private static String rawValueOf(final ThucydidesSystemProperty property) {
            String rawValue = createEnvironmentVariables().
                    getProperty(property);
            LOG.debug("Loaded [{}] Serenity BDD property with raw value [{}].", property, rawValue);
            return ofNullable(rawValue).
                    orElseThrow(() -> {
                        String message = format("Failed to load Serenity BDD property [%s].", property);
                        return new IllegalStateException(message);
                    });
        }
    }
}
