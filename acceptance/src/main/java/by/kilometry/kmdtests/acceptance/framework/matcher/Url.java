package by.kilometry.kmdtests.acceptance.framework.matcher;

import com.google.common.base.Function;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.scheduling.ThucydidesFluentWait;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

import static java.lang.String.format;
import static java.lang.invoke.MethodHandles.lookup;

/**
 * This class doesn't contain pure Hamcrest matchers but provides methods that are useful in combinations with
 * Selenium's fluent waits, e.g with {@link PageObject#waitForCondition()} and {@link
 * ThucydidesFluentWait#until(com.google.common.base.Function)} methods.
 */
public class Url {
    private static final Logger LOG = LoggerFactory.getLogger(lookup().lookupClass());

    private Url() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    private static String hostFromUrl(String urlAsString) {
        try {
            URL url = new URL(urlAsString);
            return url.getHost();
        } catch (MalformedURLException e) {
            String message = format("Input string [%s] doesn't correspond to well-formed URL.", urlAsString);
            throw new IllegalStateException(message);
        }
    }

    public static Function<WebDriver, Boolean> hostIs(final String host) {
        return (WebDriver driver) -> {
            String url = driver.getCurrentUrl();
            LOG.debug("Checking for host [{}] " +
                            "in URL [{}] " +
                            "using web-driver [{}].",
                    host,
                    url,
                    driver);
            return hostFromUrl(url).
                    equals(host);
        };
    }

    public static Function<WebDriver, Boolean> urlContains(final String fragment) {
        return (WebDriver driver) -> {
            String url = driver.getCurrentUrl();
            LOG.debug("Checking for fragment [{}] " +
                            "in URL [{}] " +
                            "using web-driver [{}].",
                    fragment,
                    url,
                    driver);
            return url.contains(fragment);
        };
    }

    public static Function<WebDriver, Boolean> urlEndsWith(final String ending) {
        return (WebDriver driver) -> {
            String url = driver.getCurrentUrl();
            LOG.debug("Checking for ending [{}] " +
                            "in URL [{}] " +
                            "using web-driver [{}].",
                    ending,
                    url,
                    driver);
            return url.endsWith(ending);
        };
    }
}
