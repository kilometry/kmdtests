package by.kilometry.kmdtests.acceptance.framework.util;

import net.serenitybdd.core.pages.WebElementFacade;
import org.aspectj.lang.JoinPoint;

import static java.lang.String.format;

/**
 * Utility for graceful extraction of target objects in advices.
 * <p>
 * If a join point doesn't contain an instance of desired type then detailed exception will be thrown.
 */
public class JoinPointExtractors {
    private JoinPointExtractors() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    public static WebElementFacade webElementFacade(JoinPoint joinPoint) {
        Object target = joinPoint.getTarget();
        if (target instanceof WebElementFacade)
            return (WebElementFacade) joinPoint.getTarget();
        throw exception(joinPoint, WebElementFacade.class, target.getClass());
    }

    private static IllegalArgumentException exception(
            JoinPoint joinPoint,
            Class<?> desiredClass,
            Class<?> actualClass) {
        String message = format("" +
                        "Join point [%s] does not contain an instance of desired type. " +
                        "Expected: [%s]. " +
                        "Was: [%s].",
                joinPoint, desiredClass, actualClass);
        return new IllegalArgumentException(message);
    }
}
