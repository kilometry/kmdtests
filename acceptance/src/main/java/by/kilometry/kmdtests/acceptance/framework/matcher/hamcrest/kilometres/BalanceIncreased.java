package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.kilometres;

import by.kilometry.kmdtests.acceptance.framework.matcher.highlighting.Explicit;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.Description;

import static java.lang.String.format;

public class BalanceIncreased extends BalanceMatcher {
    private final int oldBalance;
    private final int expectedRise;

    private WebElementFacade element;

    private BalanceIncreased(int oldBalance, int expectedRise) {
        this.oldBalance = oldBalance;
        this.expectedRise = expectedRise;
    }

    @Override
    protected boolean matchesSafely(final WebElementFacade element) {
        this.element = element;
        Explicit.highlight(element);
        actualBalance = balanceFromWebElement(element);
        if (actualBalance != oldBalance + expectedRise) {
            return false;
        }
        Explicit.putAway(element);
        return true;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("a web-element [").
                appendText(element.toString()).
                appendText("] ").
                appendText("with balance in kilometres [").
                appendText(format("%s", oldBalance + expectedRise)).
                appendText("]");
    }

    @Override
    protected void describeMismatchSafely(WebElementFacade ignored, Description mismatchDescription) {
        mismatchDescription.appendText("balance was [").
                appendText(format("%s", actualBalance)).
                appendText("]");
    }

    public static class BalanceIncreasedMatcherBuilder {
        private int oldBalance;

        private BalanceIncreasedMatcherBuilder() {
            // Prevent usage of constructor outside of the builder,
            // e.g. prevent instantiation of the builder outside
            // our small DSL.
            //
            // This will also help to get `oldBalance' field always
            // initialized (because there will not be other way to
            // set the field rather then using `increasedFrom()'
            // static method).
        }

        public static BalanceIncreasedMatcherBuilder increasedFrom(int oldBalance) {
            BalanceIncreasedMatcherBuilder builder = new BalanceIncreasedMatcherBuilder();
            builder.oldBalance = oldBalance;
            return builder;
        }

        public BalanceIncreased by(int expectedRise) {
            return new BalanceIncreased(this.oldBalance, expectedRise);
        }
    }
}


