package by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.kilometres;

import by.kilometry.kmdtests.acceptance.framework.matcher.hamcrest.WebElementMatcher;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.concurrent.TimeUnit;

import static by.kilometry.kmdtests.acceptance.framework.util.Parsing.kilometres;
import static java.lang.String.format;

public abstract class BalanceMatcher extends WebElementMatcher {
    int actualBalance;

    public static int balanceFromWebElement(final WebElementFacade balance) {
        // Once profile page is loaded, zero is displayed as balance.
        // A little bit later actual value of kilometres replaces
        // zero (via JavaScript). So this explicit nasty delay is
        // absolutely necessary.
        try {
            TimeUnit.SECONDS.sleep(TIMEOUT_FOR_NEW_BALANCE_IN_SECONDS);
        } catch (InterruptedException e) {
            String message = format("Interrupted while waiting " +
                            "for new balance in element [%s] " +
                            "with timeout of [%d] seconds.",
                    balance,
                    TIMEOUT_FOR_NEW_BALANCE_IN_SECONDS);
            throw new IllegalStateException(message, e);
        }
        return kilometres(balance.getText());
    }

    private static final int TIMEOUT_FOR_NEW_BALANCE_IN_SECONDS = 5;
}
