package by.kilometry.kmdtests.acceptance.framework.util;

import org.jbehave.core.model.ExamplesTable;

import java.util.List;

import static by.kilometry.kmdtests.acceptance.framework.util.StreamApi.toUnmodifiableList;

/**
 * Handy shortcuts for converting JBehave example tables into pure Java collections.
 * <p>
 * Returned collection are immutable.
 */
public class NamedParameters {
    private NamedParameters() {
        throw new UnsupportedOperationException("This is utility class. Use its static methods only.");
    }

    public static List<String> asList(ExamplesTable oneColumnTable, String columnName) {
        return oneColumnTable.getRows().
                stream().
                map(row -> row.get(columnName)).
                collect(toUnmodifiableList());
    }
}
