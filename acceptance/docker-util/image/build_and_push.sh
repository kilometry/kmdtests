#!/usr/bin/env bash

readonly account='yuriysechko'
readonly repository='selenium-java-maven'
readonly tag='1.0.0'

docker build --tag="$account/$repository:$tag" .

docker push  "$account/$repository:$tag"