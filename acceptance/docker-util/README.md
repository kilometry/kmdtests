# Utilities for Docker

This folder contains various stuff that simplifies working with Docker. 

At this time all of the utilities are just shell-scripts and intended to be ran on Linux. Feel free to contribute and
add support for other operating systems.

## image

Utilities for building custom image for Acceptance Tests. Currently the image includes Java 8 and Maven build 
tool in addition to Selenium.