#!/usr/bin/env bash

set -x
set -e

readonly selenium_executable='/opt/bin/entry_point.sh'
readonly selenium_log='/tmp/selenium.log'
readonly selenium_readiness_indicator='Selenium Server is up and running on port 4444'

function selenium_in_background {
    "$selenium_executable" > "$selenium_log" 2>&1 &
}

function wait_for_selenium {
    (tail --follow "$selenium_log" &) | \
    grep --quiet "$selenium_readiness_indicator"
}

function maven {
    export JAVA_HOME='/usr/lib/jvm/java-8-openjdk-amd64'
    export PATH="$JAVA_HOME/bin:$PATH"
    # Building the project with superuser privileges is poor practice...
    # However, we don't run a daemon that exposes network ports. Running
    # Maven isn't so critical. But passing environment variables is a bit
    # tricky due to `sudo'. Anyway, the following command is a subject for future
    # improvement.
    sudo --preserve-env mvn clean verify -Pdocker.internal "$@"
}

selenium_in_background
wait_for_selenium
maven "$@"