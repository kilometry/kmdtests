# Acceptance

This is a suite designed in order to run Acceptance Tests for the website and get pretty test reports.

The suite is built upon [Java](https://www.oracle.com/java/index.html) and 
[Serenity BDD](http://www.thucydides.info/#/) technologies. So it has some drawbacks as well as advantages. 

If you would like to get visual notion about the testing process please watch the video (select 720p quality for reading 
console output):

[![Thumbnail of the video](https://bitbucket.org/yuriysechko/yuriysechko.bitbucket.io/raw/01345b6ed8903ee79b5e25f1eb38186846508541/thumbnail_for_video.png)](https://www.youtube.com/watch?v=feq9VGiSO1A&)

## Running the tests

### Entirely inside a Docker-container

This approach means that compilation of the source code and execution of the tests will be done inside a container. 

The process is very time-consuming because all the third-party software will be downloaded every time you run the suite.
But you haven't to care about the environment: it will be prepared inside container automatically.

In order to get the tests running execute a shell script according to you OS. On Unix it will be:

```bash
./acceptance.docker
```

While on Windows you can run:

```bat 
acceptance.docker
```

### Partially inside a Docker-container

This option lets you run only Selenium standalone server inside a container. Other routines (like compilation of the 
source code) will be executed on a host machine.

The process is rather quick. But before running the suite you need to install **JDK 8** and 
[Maven](https://maven.apache.org/) on host machine.

To run the tests just apply to Maven build tool (the command line is identical in case of any OS):

```
mvn -Pdocker clean verify
```

`-Pdocker` stands for specific Maven's profile (please read 
[Introduction to Build Profiles](http://maven.apache.org/guides/introduction/introduction-to-profiles.html)
for more details). In future, more profiles can be introduced without affecting profiles which exist already.

### Entirely on a host machine

With this option you can compile and run the tests on your computer. In addition to Java stuff mentioned in the previous 
paragraph, you need a web-browser installed. Currently only *Chrome* is supported. Also, the location of web-driver 
should be specified via command line, like this (the example is given for Linux, file separators can be different in 
your particular OS):

```
mvn clean verify -Plocal.chrome -Dwebdriver.chrome.driver=/path/to/chrome-linux/chromedriver
```

### Run specific tests

Running the whole test suite takes long time. If you would like to execute only tests that correspond to certain 
scenarios please appeal to [Meta Filtering](http://jbehave.org/reference/stable/meta-filtering.html) mechanism. For 
example, in order to run only tests related to user's registration functionality specify `+registration` filter on 
command line. Like this:

```
mvn clean verify -Pdocker -Dmetafilter="+registration"
``` 

This also works in case of running the tests entirely inside a Docker-container. Same filter as above:

```bash
./acceptance.docker -Dmetafilter="+registration"
```

WARNING: Be aware that **all the rest** of command line after script name (e.g. `acceptance.docker`) will be treated as
shell command line arguments and passed to Maven build tool (that's running inside a container) "as is". For instance, 
the following command will result in *Non-readable POM /some/file: /some/file* error:

```bash
./acceptance.docker --file /some/file
```

`--file` is Maven's option that overrides location of so-called POM-file (the entry point to the build). The option was 
propagated to Maven inside a container literally.

## Viewing test reports

The reports will be placed into `target/site/serenity` folder under current working directory. In order to view them 
just open home page of the reports (e.g. `index.html` file) in your favourite browser, for example, Mozilla Firefox:

```
firefox target/site/serenity/index.html
```

You will see page like this:

!["Overview of test reports"](https://bitbucket.org/yuriysechko/yuriysechko.bitbucket.io/raw/1e10a89def160a27d47501caf60a24a3ff726130/overview_of_test_reports.png)